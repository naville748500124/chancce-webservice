<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Usuários</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/7">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Usuário
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th style="width: 60px;" align="center" class="no-filter">Editar</th>
		<th>ID</th>
		<th>Nome</th>
		<th>Login</th>
		<th>E-mail</th>
		<th>Status</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais['usuarios'] as $usuario) {
			echo '<tr>';

			echo '<td style="width: 60px;">
					<a class="btn btn-success" href="'.base_url().'main/redirecionar/9/'.$usuario->id_usuario.'"> <i class="glyphicon glyphicon-edit"> </i> Editar
					</button>
				</td>';

			echo '<td>'.$usuario->id_usuario.'</td>';
			echo '<td>'.$usuario->nome_usuario.'</td>';
			echo '<td>'.$usuario->login_usuario.'</td>';
			echo '<td>'.$usuario->email_usuario.'</td>';
			if ($usuario->ativo_usuario) {
				echo '<td>Ativo</td>';
			} else {
				echo '<td>Inativo</td>';
			}

			echo '</tr>';
		}

	?>
	</tbody>
</table>
