<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_oportunidades extends CI_Model {

		function __construct() {
		    parent::__construct();
		    $this->db->query("SET time_zone='-3:00'");
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### Querys ###############################

		public function view_oportunidades(){

			return $this->db->query("SELECT
										date_format(data_bloco,'%d/%m/%Y') as data_bloco,
										concat('bloco ',cb.id_bloco) as bloco,
										cb.data_bloco_usuario,
										cb.qtd_canhotos,
										cb.fk_usuario,
										su.nome_usuario,
										cb.data_fim_bloco_usuario,
										count(cc.auditoria_1) as auditado1,
										count(cc.auditoria_2) as auditado2,
										cb.valor_canhoto
										FROM
										cad_blocos cb
										INNER JOIN cad_canhotos cc ON (cb.id_bloco = cc.fk_bloco_canhoto)
										LEFT JOIN seg_usuarios su ON (cb.fk_usuario = su.id_usuario)
										WHERE
										cb.data_bloco_usuario IS NOT NULL
										GROUP BY cb.id_bloco")->result();

		}
		

	}

?>
