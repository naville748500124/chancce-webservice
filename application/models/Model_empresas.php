<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_empresas extends CI_Model {

		function __construct() {
		    parent::__construct();
		    $this->db->query("SET time_zone='-3:00'");
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### Querys ###############################

		public function view_empresas(){

			return $this->db->query('select id_empresa, nome_fantasia_empresa, email_empresa, cnpj_empresa , status_empresa
									from cad_empresa')->result();

		}

		public function view_editar_empresa($where = null){
			
			$empresa = $this->db->get_where('cad_empresa', array('id_empresa' => $where[0]))->row();

			if (isset($empresa)) {
				foreach ($empresa as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			return array('filiais' => $this->db->query('select * from cad_filial where fk_empresa = '.$where[0])->result(),
						 'motoristas' => $this->db->query('select id_filial,id_ws_motorista,id_motorista,nome_motorista,endereco_motorista, nome_filial 
						 									from cad_motorista 
						 									inner join cad_filial on id_filial = fk_filial
						 									where fk_empresa =  '.$where[0])->result());

		}

		public function create($valores = null){

			$this->db->insert('cad_empresa',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_empresas / create';
				return false;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function update($valores = null){

			//Alterar
			$tabela = "cad_empresa";
			$id = 'id_empresa';
			
			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'original_edicao'=> $comparar[$key],
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				}
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_empresas / update';
				return false;		
			} else {
				return true;
			}

		}

		public function cadFilial($valores = null){

			$this->db->insert('cad_filial',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_empresas / cadFilial';
				return false;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function edtFilial($valores = null){

			//Alterar
			$tabela = "cad_filial";
			$id = 'id_filial';
			
			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'original_edicao'=> $comparar[$key],
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				}
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_empresas / edtFilial';
				return false;		
			} else {
				return true;
			}


		}

		public function cadMotorista($valores = null){

			$this->db->insert('cad_motorista',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_empresas / cadMotorista';
				return false;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function editMotorista($valores = null){

			//Alterar
			$tabela = "cad_motorista";
			$id = 'id_motorista';
			
			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'original_edicao'=> $comparar[$key],
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				}
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_empresas / edtFilial';
				return false;		
			} else {
				return true;
			}

		}
		

	}

?>