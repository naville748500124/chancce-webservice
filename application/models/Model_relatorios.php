<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_relatorios extends CI_Model {

		private $code = null;
		private $message = null;
		private $query = null;
		private $funcao = null;

		function __construct() {
		    parent::__construct();
		    $this->db->query("SET time_zone='-3:00'");
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### QUERYS ###############################

		public function view_relatorio_xls(){

		return $this->db->query("SELECT 
									nome_filial,
									nome_motorista,
									date_format(data_canhoto,'%d/%m/%Y') as data_canhoto, 
									count(cc0.id_canhoto) as total,

									(SELECT count(cc1.id_canhoto) as acertos
										FROM cad_canhotos cc1
										LEFT JOIN cad_erro_auditoria_1 cea1 ON (cc1.id_canhoto = cea1.fk_canhoto)
										LEFT JOIN cad_erro_auditoria_2 cea2 ON (cc1.id_canhoto = cea2.fk_canhoto)
										LEFT JOIN cad_motorista cm ON (cc1.ws_motorista_canhoto = cm.id_ws_motorista)
										WHERE ((cc1.auditoria_1 = 1 AND cc1.auditoria_2 = 1) OR (cea2.fk_erro = 3))
										AND cc1.ws_motorista_canhoto = cc0.ws_motorista_canhoto
										/*AND cm.fk_filial = 7*/
										/*AND data_canhoto BETWEEN '2017-09-14' AND '2017-09-17'*/
										GROUP BY cm.fk_filial, cc1.ws_motorista_canhoto) as  acertos,

									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 3) as c3,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 4) as c4,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 5) as c5,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 6) as c6,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 7) as c7,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 8) as c8,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 9) as c9,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 10) as c10,
									(select qtd_erros from conta_erros where ws_motorista_canhoto = cc0.ws_motorista_canhoto and id_item = 11) as c11

									FROM cad_canhotos cc0
									LEFT JOIN cad_motorista cm ON (cc0.ws_motorista_canhoto = cm.id_ws_motorista)
									inner JOIN cad_filial on id_filial = cm.fk_filial
									WHERE (cc0.auditoria_1 IS NOT NULL AND cc0.auditoria_2 IS NOT NULL)
									/*AND cc0.ws_motorista_canhoto = 50*/
									/*AND cm.fk_filial = 7*/
									/*AND data_canhoto BETWEEN '2017-09-14' AND '2017-09-17'*/
									GROUP BY cm.fk_filial, cc0.ws_motorista_canhoto")->result();

		}

		public function view_relatorio(){

			return $this->db->query("SELECT 
										nome_filial,
										nome_motorista,
										date_format(data_canhoto,'%d/%m/%Y') as data_canhoto, 
										count(cc0.id_canhoto) as total,

										(SELECT count(cc1.id_canhoto) as acertos
											FROM cad_canhotos cc1
											LEFT JOIN cad_erro_auditoria_1 cea1 ON (cc1.id_canhoto = cea1.fk_canhoto)
											LEFT JOIN cad_erro_auditoria_2 cea2 ON (cc1.id_canhoto = cea2.fk_canhoto)
											LEFT JOIN cad_motorista cm ON (cc1.ws_motorista_canhoto = cm.id_ws_motorista)
											WHERE ((cc1.auditoria_1 = 1 AND cc1.auditoria_2 = 1) OR (cea2.fk_erro = 3))
											AND cc1.ws_motorista_canhoto = cc0.ws_motorista_canhoto
											/*AND cm.fk_filial = 7*/
											/*AND data_canhoto BETWEEN '2017-09-14' AND '2017-09-17'*/
											GROUP BY cm.fk_filial, cc1.ws_motorista_canhoto) as  acertos

										FROM cad_canhotos cc0
										LEFT JOIN cad_motorista cm ON (cc0.ws_motorista_canhoto = cm.id_ws_motorista)
										inner JOIN cad_filial on id_filial = cm.fk_filial
										WHERE (cc0.auditoria_1 IS NOT NULL AND cc0.auditoria_2 IS NOT NULL)
										/*AND cc0.ws_motorista_canhoto = 50*/
										/*AND cm.fk_filial = 7*/
										/*AND data_canhoto BETWEEN '2017-09-14' AND '2017-09-17'*/
										GROUP BY cm.fk_filial, cc0.ws_motorista_canhoto")->result();

		}

		public function view_relatorio_excel(){



		}


	}