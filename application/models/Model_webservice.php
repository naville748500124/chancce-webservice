<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_webservice extends CI_Model {

		############################### TRANSACTION ###############################
		public function start(){

			$this->db->trans_begin();

		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){

			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			  	$erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {

			    $this->db->trans_commit();
			    return array('status' => true);

			}

		}


		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}

		############################### Querys ###############################

		function __construct() {

		    parent::__construct();
		    //Define o TIMEZONE para o de SP
			date_default_timezone_set('America/Sao_Paulo');
			$this->db->query("SET time_zone='-3:00'");
			    
		}

		public function validar_login($dados = null){

			$query = 'select *, (SELECT INSERT( INSERT( INSERT( cpf_usuario, 10, 0, \'-\' ), 7, 0, \'.\' ), 4, 0, \'.\' )) as cpf_usuario, top_user as top,
			 (select count(*) as badge_blocos
								from cad_blocos
								where (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) > 0
								and fk_usuario = id_usuario and expirou_bloco = false) as badge_blocos,
								CONCAT (
								        \'(\',
								        SUBSTR(telefone_usuario,1,2),\') \',
								        SUBSTR(telefone_usuario,3,5),\'-\',
								        SUBSTR(telefone_usuario,8)) as telefone_usuario
								        from seg_usuarios
								        inner join seg_grupos on fk_grupo_usuario = id_grupo
								        where email_usuario = \''.$dados['email'].'\'
								        and senha_usuario = \''.$dados['senha'].'\'';

			$login = $this->db->query($query)->row_array();

			if (isset($login) && $login['ativo_usuario']) {

				//Histórico de acesso
				$dados = array(

					'fk_usuario' => $login['id_usuario'],
					'ip_usuario_acesso' => $_SERVER['REMOTE_ADDR'],
					'maquina_usuario_acesso' => $_SERVER['HTTP_USER_AGENT'],
					'acesso' => true

				);

			$this->db->insert('seg_log_acesso',$dados);
			$login['id_acesso'] = $this->db->insert_id();

			$this->verificar_top($login['id_usuario']);

				return $this->db->query($query)->row_array();
			} else {
				return false;
			}

		}

		public function loginFacebookApp($id_facebook = null){

			$query = "select *, (SELECT INSERT( INSERT( INSERT( cpf_usuario, 10, 0, '-' ), 7, 0, '.' ), 4, 0, '.' )) as cpf_usuario, top_user as top,
			 (select count(*) as badge_blocos
								from cad_blocos
								where (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) > 0
								and fk_usuario = id_usuario and expirou_bloco = false) as badge_blocos,
								CONCAT (
								        '(',
								        SUBSTR(telefone_usuario,1,2),') ',
								        SUBSTR(telefone_usuario,3,5),'-',
								        SUBSTR(telefone_usuario,8)) as telefone_usuario
								        from seg_usuarios
								        inner join seg_grupos on fk_grupo_usuario = id_grupo
								        where id_facebook = '{$id_facebook}'";

			$login = $this->db->query($query)->row_array();

			if (isset($login) && $login['ativo_usuario']) {

				//Histórico de acesso
				$dados = array(

					'fk_usuario' => $login['id_usuario'],
					'ip_usuario_acesso' => $_SERVER['REMOTE_ADDR'],
					'maquina_usuario_acesso' => $_SERVER['HTTP_USER_AGENT'],
					'acesso' => true

				);

			$this->db->insert('seg_log_acesso',$dados);
			$login['id_acesso'] = $this->db->insert_id();

			$this->verificar_top($login['id_usuario']);

				return $this->db->query($query)->row_array();
			} else {
				return false;
			}

		}

		public function set_($campo,$valor){
			$this->$campo = $valor;
		}

		public function get_($campo){
			return $this->$campo;
		}

		public function validarEmail($email = null){

			return $this->db->query("select (count(*) > 0) as existe from seg_usuarios where email_usuario = '{$email}';")->row()->existe;

		}

		public function validarCpf($cpf = null){

			return $this->db->query("select (count(*) > 0) as existe from seg_usuarios where cpf_usuario = '{$cpf}';")->row()->existe;

		}

		//Usado no APP na tela de novo cadastro e Edição do cadastro
		public function listarBancos(){

			return $this->db->query('select id_item as idBanco, nome_item as nomeBanco 
										from cad_itens 
										inner join cad_grupo_itens on id_grupo_item = fk_grupo_item
										where id_grupo_item = 1')->result_array();

		}

		public function novoUsuario($valores = null){

			$this->db->insert('seg_usuarios',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'Model_webservice / novoUsuario';
				return false;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function atualizarUsuario($valores = null) {
			//Alterar
			$tabela = "seg_usuarios";
			$id = 'id_usuario';

			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'original_edicao'=> $comparar[$key],
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				} 
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_webservice / atualizarUsuario';
				return false;		
			} else {
				return true;
			}

		}

		public function senha_Email($senha = null, $email = null){

			$existe = $this->db->query("select count(*) as qtd from seg_usuarios where email_usuario = '{$email}'")->row()->qtd;

			if ($existe == 1) {
				return $this->db->query("update seg_usuarios set senha_usuario = '{$senha}' where email_usuario = '{$email}'");
			} else {
				return false;
			}


		}

		public function novaSenha($senha = null){

			return $this->db->query("update seg_usuarios set senha_usuario = '{$senha['senha_usuario']}' where id_usuario = {$senha['id_usuario']} and senha_usuario = '{$senha['senha_usuario_atual']}'");

		}

		/*Samuel Serapiãoo 09/10/2017
		Função alterada para exibir blocos de 400 para os usuários top*/
		public function listarBlocos($id_usuario = null){
			
			$valorPorUsuario = $this->db->query("select valor from seg_usuarios inner join  cad_valor_canhoto on fk_valor_usuario = id_valor where id_usuario = {$id_usuario}")->row()->valor;

			if ($this->db->query("select top_user from seg_usuarios where id_usuario = {$id_usuario}")->row()->top_user) {
				return $this->db->query('select cb.id_bloco, (cb.qtd_canhotos * '.$valorPorUsuario.') as valor, date_format(cb.data_bloco,\'%d/%m/%Y\') as data_bloco from cad_blocos cb left join cad_user_bloco cub on (cb.id_bloco = cub.fk_bloco and cub.fk_usuario = '.$id_usuario.') where cb.fk_usuario is null and cub.fk_bloco is null and date(cb.data_bloco) = DATE_SUB(curdate(), interval 2 day) order by cb.id_bloco LIMIT 25')->result();
			} else {
				return $this->db->query('select cb.id_bloco, (cb.qtd_canhotos * '.$valorPorUsuario.') as valor, date_format(cb.data_bloco,\'%d/%m/%Y\') as data_bloco from cad_blocos cb left join cad_user_bloco cub on (cb.id_bloco = cub.fk_bloco and cub.fk_usuario = '.$id_usuario.') where cb.fk_usuario is null and cub.fk_bloco is null and date(cb.data_bloco) = DATE_SUB(curdate(), interval 2 day) and qtd_canhotos < 400 order by cb.id_bloco LIMIT 25')->result();
			}
		}

		//Exibir canhoto atual
		public function lerCanhotos($id_bloco = null){

			$id_usuario = $this->db->query("select fk_usuario as id_usuario 
												from cad_blocos 
												where id_bloco = {$id_bloco}")->row()->id_usuario;
			//Caso acabe no meio.
			$this->eliminarBlocos($id_usuario);

			$canhoto = $this->db->query("

				select 
					ws_id_canhoto as id_canhoto, 
					concat('http://mobile.diaslog.com.br/api/Canhoto/',ws_id_canhoto) as img_link,
					date_format(data_bloco,'%d/%m/%Y') as data_canhoto,
					((select count(*) from cad_canhotos cc0 where cc0.fk_bloco_canhoto = cc.fk_bloco_canhoto and auditoria_1 is not null and auditoria_2 is null) + 1) canhoto_atual,
					(select count(*) from cad_canhotos cc0 where cc0.fk_bloco_canhoto = cc.fk_bloco_canhoto) canhoto_total,
			        
			        case 
			                
			                when (select concat('http://mobile.diaslog.com.br/api/Canhoto/',ws_id_canhoto) as img_link
						from cad_canhotos cc1
			            where cc1.fk_bloco_canhoto = {$id_bloco}
							and cc1.auditoria_1 is null
			                and cc1.id_canhoto > cc.id_canhoto
							limit 1)  is null 

					then (select concat('http://mobile.diaslog.com.br/api/Canhoto/',ws_id_canhoto) as img_link
												from cad_canhotos cc1
												where cc1.fk_bloco_canhoto = {$id_bloco}
												limit 1) 
			                else 
			                (select concat('http://mobile.diaslog.com.br/api/Canhoto/',ws_id_canhoto) as img_link
								from cad_canhotos cc1
								where cc1.fk_bloco_canhoto = {$id_bloco}
									and cc1.auditoria_1 is null
									and cc1.id_canhoto > cc.id_canhoto
									limit 1)  end as img_link_proximo
			        
					from cad_canhotos cc
					inner join cad_blocos on id_bloco = fk_bloco_canhoto
					where fk_bloco_canhoto = {$id_bloco}
					and expirou_bloco = 0
					and auditoria_1 is null
					limit 1;"

				);

			//Não existe um próximo canhoto
			if ($canhoto->num_rows() == 0) {
				
				$this->db->query('update cad_blocos 
									set expirou_bloco = 1, data_fim_bloco_usuario = current_time 
										where id_bloco = '.$id_bloco);

				return $canhoto->row_array();

			} else {
				
				return $canhoto->row_array();

			}

		}

		//Sinaliza que o usuário escolheu esse bloco
		public function selecionouBloco($dados = null){

			$this->db->query("update cad_blocos set fk_usuario = {$dados['fk_usuario']}, valor_canhoto = (select valor 
													from seg_usuarios 
													inner join  cad_valor_canhoto on fk_valor_usuario = id_valor
													where id_usuario = {$dados['fk_usuario']}), data_bloco_usuario = current_time 
										where id_bloco = {$dados['id_bloco']}");

			return $this->db->query('select count(*) as badge_blocos
								from cad_blocos
								where (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) > 0
								and expirou_bloco = false
								and fk_usuario = '.$dados['fk_usuario'])->row();
		}

		//Listar Blocos em andamento por usuário, exibindo tempo restante, remover blocos que expiraram.
		public function listarBlocosUsuario($usuario = null){

			$this->eliminarBlocos($usuario);

			return $this->db->query("select id_bloco, (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) as tempo_restante, 
										date_format(data_bloco,'%d/%m/%Y') as data_bloco, 
														(select count(*) as badge_blocos
															from cad_blocos
															where (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) > 0
															and fk_usuario = {$usuario} and expirou_bloco = false) as badge_blocos   
										from cad_blocos
										where (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) > 0
										and expirou_bloco = false
										and date(data_bloco) = DATE_SUB(curdate(), interval 2 day)
										and fk_usuario = {$usuario}")->result();

		}

		//Pega todos blocos ainda ativos e muda seu status, por cliente.
		public function eliminarBlocos($usuario = null){

			/*Samuel Serapiao 27/09/2017
			Verifica se expirou o tempo do bloco, limpa a auditoria do usuario
			*/
			$this->db->query("update cad_blocos set expirou_bloco = true 
								where
								date(data_bloco) <  DATE_SUB(curdate(), interval 2 day))
								and fk_usuario = {$usuario}
								and expirou_bloco = false");

			$blocos = $this->db->query("select id_bloco from cad_blocos where (7200 - TIME_TO_SEC(timediff(current_time(),data_bloco_usuario))) < 0 and fk_usuario = {$usuario}")->result();

			foreach ($blocos as $key => $bloco) {
				$this->db->query("update cad_canhotos set auditoria_1 = null, auditoria_2 = null, fk_auditor = null where fk_bloco_canhoto = {$bloco->id_bloco}");
				$this->db->query("update cad_blocos set fk_usuario = null, data_bloco_usuario = null, valor_canhoto = null where id_bloco = {$bloco->id_bloco}");
				$this->db->query("insert into cad_user_bloco (fk_bloco, fk_usuario) values ({$bloco->id_bloco}, {$usuario})");

				$canhotos = $this->db->query("select id_canhoto from cad_canhotos where fk_bloco_canhoto	 = {$bloco->id_bloco}")->result();
				foreach ($canhotos as $key => $canhoto) {
					$this->db->query("delete from cad_erro_auditoria_1 where fk_canhoto = {$canhoto->id_canhoto}");
					$this->db->query("delete from cad_erro_auditoria_2 where fk_canhoto = {$canhoto->id_canhoto}");
				}
			}

			$this->verificar_top($usuario);


		}

		public function verificar_top($usuario = null){
			//Verifica se o usuário virou top
			$top = $this->db->query("SELECT
										cc.fk_bloco_canhoto,
										cc.auditoria_2,
										cb.qtd_canhotos,
										count(cc.auditoria_2),
										CASE WHEN cc.auditoria_2 = 1 AND cb.qtd_canhotos = count(cc.auditoria_2) THEN 1
										    ELSE 0 END AS top_user
										FROM
											cad_canhotos cc
										LEFT JOIN cad_blocos cb ON (cc.fk_bloco_canhoto = cb.id_bloco)
										WHERE cc.fk_auditor = {$usuario} and cc.auditoria_2 is not null
										GROUP BY cc.fk_bloco_canhoto, cc.auditoria_2
										ORDER BY cb.data_bloco, cb.data_bloco_usuario DESC
										LIMIT 5")->result();

			$virou_top = 0;
			//São somadas as 5 linhas de resultado, sendo que cada bloco 100% correto retorna 1
			foreach ($top as $t) {
				$virou_top += $t->top_user;
			}

			//Caso a soma de 5 ele ganha um level up para TOP User
			if ($virou_top == 5) {
				$this->db->query("update seg_usuarios set top_user = true where id_usuario = {$usuario}");
			} else { //Caso seja top hoje, mas algum bloco marque erro.
				$this->db->query("update seg_usuarios set top_user = false where id_usuario = {$usuario}");
			}
		}

		public function atualizarCanhoto($dados = null){

			$opcoes = explode(',', $dados['opcoes']);

			if ($opcoes[0] == 3) {//Marcou OK no canhoto
				//Atualiza Canhoto
				$this->db->query('update cad_canhotos set auditoria_1 = 1, fk_auditor = '.$dados['id_usuario'].' where ws_id_canhoto = '.$dados['ws_id_canhoto']);
			} else {
				$this->db->query('update cad_canhotos set auditoria_1 = 2, fk_auditor = '.$dados['id_usuario'].' where ws_id_canhoto = '.$dados['ws_id_canhoto']);

			}

			foreach ($opcoes as $key => $opcao) {

				$this->db->query("insert into cad_erro_auditoria_1 (fk_canhoto,fk_erro) 
									 values ((select id_canhoto from cad_canhotos where ws_id_canhoto = {$dados['ws_id_canhoto']}),{$opcao})");

			}

			return $this->lerCanhotos($dados['id_bloco']);

		}
		
		public function validarData($data = null){

			return $this->db->query("select count(*) as existe from cad_blocos where date(data_bloco) = '{$data}'")->row()->existe;

		}

		/*Samuel Serapião 09/10/2017
		Função alterada para gravar blocos para usuários top*/
		public function gravarCanhotos($canhotos = null) {

			$data = date('Y-m-d', strtotime('-2 days', strtotime(date('Y-m-d'))));
			$id_bloco = $this->novo_Bloco();

			$qtd_top = $this->db->query("select count(top_user) as qtd_top from seg_usuarios where top_user = true and ativo_usuario = true")->row()->qtd_top;

			$qtd_canhotos = 0;
			foreach ($canhotos as $canhoto) {

				$qtd_canhotos += 1;
				
				$this->db->query("insert into cad_canhotos (ws_id_canhoto,ws_motorista_canhoto,data_canhoto,fk_bloco_canhoto) values ({$canhoto->idCanhoto},{$canhoto->idMotorista},'{$data}',{$id_bloco})");

				if ($qtd_top > 0) {
					if ($qtd_canhotos % 400 == 0) {
					$this->qts_Canhotos_Bloco($id_bloco,$qtd_canhotos);
					$id_bloco = $this->novo_Bloco();
					$qtd_canhotos = 0;
					$qtd_top -= 1;
					}
				} else if ($qtd_canhotos % 5 == 0) {
					$this->qts_Canhotos_Bloco($id_bloco,$qtd_canhotos);
					$id_bloco = $this->novo_Bloco();
					$qtd_canhotos = 0;
				}

			}

			//Chamado fora para quando um bloco não chegar no mínimo estipulado.
			return $this->qts_Canhotos_Bloco($id_bloco,$qtd_canhotos);


		}

		//Gera um novo bloco de 200 canhotos.
		public function novo_Bloco(){

			$data = date('Y-m-d h:i:s', strtotime('-2 days', strtotime(date('Y-m-d h:i:s'))));

			$this->db->insert('cad_blocos',array('qtd_canhotos' => 0, 'data_bloco' => $data));
			return $this->db->insert_id();

		}

		//Atualiza o contador no canhoto
		public function qts_Canhotos_Bloco($id_bloco = null,$quantidade = null){

			$this->db->query("update cad_blocos set qtd_canhotos = {$quantidade} where id_bloco = {$id_bloco}");

		}

		//Lista dados por bloco auditado
		public function blocoAuditado($id_usuario = null){

			return $this->db->query("select 
										id_bloco,
										date_format(data_bloco,'%d/%m/%Y') data_bloco,
										qtd_canhotos,
										((select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario) * valor_canhoto)valor,
										(select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario) auditados

										from cad_blocos
										where expirou_bloco = 1
										and ((select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario and auditoria_1 is not null) > (select count(*) 
											from cad_canhotos poas
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario and auditoria_2 is not null))
										and fk_usuario = {$id_usuario}")->result();

		}

		//Lista dados por bloco pós auditado
		public function blocoPosAuditado($id_usuario = null){

			return $this->db->query("select 
										id_bloco,
										date_format(data_bloco,'%d/%m/%Y') data_bloco,
										qtd_canhotos,
										(qtd_canhotos * valor_canhoto) valor,
										(select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario) auditados,
										    
										(select count(*) 
											from cad_canhotos poas
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario and auditoria_2 is not null) pos_auditados,
                                            
										((select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario and auditoria_2 = 2) * valor_canhoto) valor_desconto,
										    
										((select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario and auditoria_2 = 1) * valor_canhoto) as  valor_pos_auditado

										from cad_blocos
										where expirou_bloco = 1

										and ((select count(*) 
											from cad_canhotos 
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario) = (select count(*) 
											from cad_canhotos poas
										    where fk_bloco_canhoto = id_bloco and fk_auditor = fk_usuario and auditoria_2 is not null))

										and fk_usuario = {$id_usuario}")->result();
			
		}


}



