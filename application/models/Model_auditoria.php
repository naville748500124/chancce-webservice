<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_auditoria extends CI_Model {

		function __construct() {
		    parent::__construct();
		    $this->db->query("SET time_zone='-3:00'");
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			    $erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### Querys ###############################
	
		public function view_audicoes(){

			$dados['audicoes'] = $this->db->query('select fk_bloco_canhoto,id_canhoto,ws_id_canhoto,ws_id_canhoto as img, (select GROUP_CONCAT(nome_item )
																from cad_itens 
																inner join cad_erro_auditoria_1 on fk_erro = id_item
																where fk_canhoto = id_canhoto) as opcoes from cad_canhotos left join cad_blocos on (fk_bloco_canhoto = id_bloco) where auditoria_1 is not null 
																	and auditoria_2 is null
																	and data_fim_bloco_usuario is not null
																	order by fk_bloco_canhoto
																	limit 10')->result();


			$dados['erros'] = $this->db->query("SELECT
													id_item,
													nome_item,
													descricao_item
														from cad_itens	
												    		where fk_grupo_item = 3")->result();

			return $dados;

		}

		public function badgeAuditar(){
 
			return $this->db->query('select count(*) as qtd
													from cad_canhotos 
													join cad_blocos on (fk_bloco_canhoto = id_bloco)
													where auditoria_1 is not null 
													and data_fim_bloco_usuario is not null
													and auditoria_2 is null')->row()->qtd;


		}

		public function atualizarCanhoto($canhotos = null,$checklist = null){

			if(is_null($checklist)) {

				$this->db->query('update cad_canhotos set auditoria_2 = 1, fk_pos_auditor = '.$this->session->userdata('usuario').' where id_canhoto = '.$canhotos);

			} else {

				$this->db->query('update cad_canhotos set auditoria_2 = 2, fk_pos_auditor = '.$this->session->userdata('usuario').' where id_canhoto = '.$canhotos);

				foreach ($checklist as $opcao) {
					$this->db->query('insert into cad_erro_auditoria_2 (fk_canhoto,fk_erro) 
						values ('.$canhotos.','.$opcao.');');
				}

			}

			$id_usuario = $this->db->query("select fk_auditor from cad_canhotos where id_canhoto = {$canhotos}")->row()->fk_auditor;

			$this->verificar_top($id_usuario);

		}

		public function verificar_top($usuario = null){
			//Verifica se o usuário virou top
			$top = $this->db->query("SELECT
										cc.fk_bloco_canhoto,
										cc.auditoria_2,
										cb.qtd_canhotos,
										count(cc.auditoria_2),
										CASE WHEN cc.auditoria_2 = 1 AND cb.qtd_canhotos = count(cc.auditoria_2) THEN 1
										    ELSE 0 END AS top_user
										FROM
											cad_canhotos cc
										LEFT JOIN cad_blocos cb ON (cc.fk_bloco_canhoto = cb.id_bloco)
										WHERE cc.fk_auditor = {$usuario} and cc.auditoria_2 is not null
										GROUP BY cc.fk_bloco_canhoto, cc.auditoria_2
										ORDER BY cc.id_canhoto DESC
										LIMIT 5")->result();

			$virou_top = 0;
			//São somadas as 5 linhas de resultado, sendo que cada bloco 100% correto retorna 1
			foreach ($top as $t) {
				$virou_top += $t->top_user;
			}

			//Caso a soma de 5 ele ganha um level up para TOP User
			if ($virou_top == 5) {
				$this->db->query("update seg_usuarios set top_user = true where id_usuario = {$usuario}");
			} else { //Caso seja top hoje, mas algum bloco marque erro.
				$this->db->query("update seg_usuarios set top_user = false where id_usuario = {$usuario}");
			}
		}


	}

	

?>