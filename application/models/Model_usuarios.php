<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class model_usuarios extends CI_Model {

		private $code = null;
		private $message = null;
		private $query = null;
		private $funcao = null;

		function __construct() {
		    parent::__construct();
		    $this->db->query("SET time_zone='-3:00'");
		}

		############################### TRANSACTION ###############################
		public function start(){
			$this->db->trans_begin();
		}

		//Se não houverem erros de SQL envia o commit
		public function commit(){
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();

			  	$erro = array(
			    				'fk_usuario' => $this->session->userdata('usuario'),
								'cod' => $this->code,
								'erro' => $this->message,
								'query' => $this->query,
								'funcao' => $this->funcao,
								'maquina_usuario_erro' => $_SERVER['HTTP_USER_AGENT']
			    			);
			    
			    //Gerando arquivo de erro.
			    log_message('error', 
			    			'Codigo: '.$this->code.' Mensagem: "'.$this->message.'" Query: "'.$this->query.'"');
			    
			    //Armazenando no banco o log.
			    $this->db->insert('seg_log_erro',$erro);

			    return array('status' => false, 
			    			 'log_erro' => $this->db->insert_id(),
			    			 'code' => $this->code, 
			    			 'message' => $this->message, 
			    			 'query' => $this->query);

			} else {
			    $this->db->trans_commit();
			    return array('status' => true);
			}
		}

		//Caso o erro seja detectado de outra forma,
		public function rollback(){
			$this->db->trans_rollback();
		}
		############################### Querys ###############################

		public function view_usuarios(){

			return array('usuarios' => $this->db->query("select id_usuario,nome_usuario,email_usuario,login_usuario,ativo_usuario, nome_grupo from seg_usuarios inner join seg_grupos on id_grupo = fk_grupo_usuario")->result());

		}

		public function view_novo_usuario(){
			//Lista dos grupos para o select
			return $this->db->get_where('seg_grupos',array('id_grupo <>' => 2))->result();

		}

		public function view_editar_usuario($where = null){

			$usuario = $this->db->get_where('seg_usuarios',array('id_usuario' => $where[0]))->row();

			if (isset($usuario)) {
				foreach ($usuario as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			//Lista dos grupos para o select
			return $this->db->get('seg_grupos')->result();

		}

		public function view_editar_perfil(){
			//Lista dos grupos para o select

			$usuario = $this->db->get_where('seg_usuarios',array('id_usuario' => $this->session->userdata('usuario')))->row();

			if (isset($usuario)) {
				foreach ($usuario as $key => $value) {
					$this->session->set_flashdata("{$key}_edicao",$value);
				}
			}

			//Para redirecionar corretamente quando não existem outros conteúdos a serem retornados
			return array('status' => true);

		}

		public function update($valores = null){

			//Alterar
			$tabela = "seg_usuarios";
			$id = 'id_usuario';

			$comparar = $this->db->get_where($tabela,array($id => $valores[$id]))->row_array();

			foreach ($valores as $key => $valor) {
				if ($valor != $comparar[$key]) {
					$log = array (
									'fk_usuario'=> $this->session->userdata('usuario'),
									'original_edicao'=> $comparar[$key],
									'fk_aplicacao'=> $this->session->userdata('id_aplicacao_atual'),
									'novo_edicao'=> "{$valor}",
									'campo_edicao'=> "{$key}",
									'tabela_edicao'=> $tabela,
									'id_edicao'=> $valores[$id],
								);

					$this->db->insert('seg_log_edicao',$log);
				} 
			}

			$this->db->where(array($id => $valores[$id]));
			$this->db->update($tabela,$valores);	

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_usuarios / update';
				return false;		
			} else {
				return true;
			}

		}

		public function create($valores = null){

			$this->db->insert('seg_usuarios',$valores);

			$e = $this->db->error();
			if ($e['code'] != 0) {
				$this->code = $e['code'];
				$this->message = $e['message'];	
				$this->query = $this->db->last_query();
				$this->funcao = 'model_usuarios / create';
				return false;		
			} else {
				return $this->db->insert_id();
			}

		}

		public function senha_Email($senha = null,$login = null) {

			$dados = $this->db->query('select id_usuario as id,email_usuario as email from seg_usuarios where login_usuario =  \''.$login.'\' and ativo_usuario = true;');


			if($dados->num_rows() > 0) {

				$this->db->query('update seg_usuarios set senha_usuario = \''.$senha.'\' where id_usuario = '.$dados->row()->id.';');

				$e = $this->db->error();
				if ($e['code'] != 0) {
					$this->code = $e['code'];
					$this->message = $e['message'];	
					$this->query = $this->db->last_query();
					$this->funcao = 'model_usuarios / senha_Email';

					return false;		
				} else {
					return $dados->row()->email;
				}

			} else {

				return "";

			}
				

		}


	}