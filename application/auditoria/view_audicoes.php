
<?php if (count($dados_iniciais) == 0){ ?>
	<div class="col-md-12" align="center">
		<h3>Nenhum canhoto na fila.</h3>
	</div>
<?php } else { ?>
	<h3>Canhotos para Pós Auditar</h3>
<?php } ?>

<?php echo form_open('Controller_auditoria/atualizar_Canhoto'); ?>

<?php foreach ($dados_iniciais as $key => $canhoto) {

	$opcoes = explode(',', $canhoto->opcoes);
	$marcados = "";
	foreach ($opcoes as $indice => $opcao) {
		if ($opcao == "") {
			$marcados .= "<li>OK.</li>";
		} else {
			$marcados .= "<li>{$opcao}.</li>";
		}
	}

	echo '<div class="row canhoto" cod="'.$key.'">
			<div class="col-md-9">
				<input class="input_canhoto" type="checkbox" name="canhoto[]" id="canhoto_'.$key.'" value="'.$canhoto->id_canhoto.'">
				<img src="http://mobile.diaslog.com.br/api/Canhoto/'.$canhoto->img.'" width="100%">
		    </div>
		    <div class="col-md-3">
		   		<p align="center">Marcado(s):</p>'.$marcados.'
		    </div>
		</div>
		<hr>';

} ?>
	
<?php if (count($dados_iniciais) > 0): ?>

<div style="margin-bottom: 120px;"></div>

<div class="row rodape" align="center">

	<div class="col-md-10"></div>
	<div class="col-md-2" id="countCanhotos">
		0 Canhotos Marcados
	</div>

	<div id="opcoesBasicas" style="padding: 20px;">
		
		<div class="col-md-4"></div>

		<div class="col-md-4">
			<button class="btn btn-success">
				Auditoria está correta
			</button>

			<button class="btn btn-danger" id="liberarChecklist" type="button">
				Auditoria possui erros
			</button>
		</div>


	</div>

	<div id="comErros" hidden>
	
		<div class="col-md-12">
			<small>Faça uma auditoria completa, marcando 1 ou mais canhotos e exatamentes as opções no checklist.</small>
		</div>

		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="65"> OK
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="3"> Campos em Branco
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="4"> Mesmo Recebidor em endereços distintos
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="5"> Conferência obrigatória incompleta
		</div>

		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="6"> Conferência obrigatória incompleta (SSP)
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="11"> Data divergente de baixa
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="7"> Campos preenchidos pelo motorista
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="8"> Imagem cortada
		</div>
		
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="9"> Erro de RG
		</div>
		<div class="col-md-3" align="left">
			<input type="checkbox" name="checklist[]" value="10"> Canhoto com carimbo
		</div>

		<div class="col-md-12">
			
			<div class="col-md-4" align="left" style="padding-top: 10px;">
				<a href="#" id="esconderChecklist">Voltar</a>
			</div>
			<div class="col-md-4">
				<button class="btn btn-success">Confirmar</button>
			</div>

		</div>

	</div>
	
</div>

<?php endif; ?>

<style type="text/css">

	.rodape {
		background-color: white;
		padding: : 20px;
	    position: fixed;
	    height: 80px;
	    bottom: 0;
	    left: 1%;
	    width: 100%;
	}

	img:hover {
		  -moz-transform: scale(1.1);
		  -webkit-transform: scale(1.1);
		  transform: scale(1.1);
		  transition: 0.5s;
	}

</style>

<script type="text/javascript">
	$(document).ready(function(){

		//Ao clicar na DIV já marca o checkbox.
		$('.canhoto').click(function(){

			var checkbox = $('#canhoto_'+$(this).attr('cod'));
			console.log('#canhoto_'+$(this).attr('cod'));
			checkbox.attr('checked', !checkbox.attr('checked'));

			var canhotos = 0;

			$('.input_canhoto').each(function(){
				if ($(this).attr('checked')) {
					canhotos += 1; 
				}
			});

			$('#countCanhotos').text(canhotos+' Canhotos Marcados');

		});

		$('#liberarChecklist').click(function(){

			$('#comErros').show();
			$('#opcoesBasicas').hide();

			$('.rodape').css('height','150px');

		});

		$('#esconderChecklist').click(function(){

			$('#comErros').hide();
			$('#opcoesBasicas').show();

			$('.rodape').css('height','80px');

		});


	});
</script>

<?php echo form_close(); ?>