<?php defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_webservice extends CI_Controller {

		function __construct() {

		    parent::__construct();
		    $this->load->model('model_webservice');
		    //Define o TIMEZONE para o de SP
			date_default_timezone_set('America/Sao_Paulo');
			    
		}
	
		public function loginApp(){

			$dados = array(

				'email' => $this->input->post('emailUsuario'),
				'senha' => $this->input->post('senhaUsuario')

			);

			$login = $this->model_webservice->validar_login($dados);
			
			$usuario = array (
				'idUsuario' => 0
			);

			if ($login) {

				$usuario["idUsuario"] = $login["id_usuario"];
	            $usuario["nomeUsuario"] = $login["login_usuario"];
	            $usuario["visualizacaoNomeUsuario"] = $login["nome_usuario"];
	            $usuario["emailUsuario"] = $login["email_usuario"];
	            $usuario["telefoneUsuario"] = $login["telefone_usuario"];
	            $usuario["bancoUsuario"] = $login["banco_usuario"];
	            $usuario["agenciaUsuario"] = $login["agencia_usuario"];
	            $usuario["contaUsuario"] = $login["conta_usuario"];
		        $usuario["digitoUsuario"] = $login["digito_usuario"];
	            $usuario["cpfUsuario"] = $login["cpf_usuario"];
	            $usuario["badge_blocos"] = $login["badge_blocos"];
	            $usuario["top"] = $login["top"];
	            
	            //Atualizar código
	            $urlImagem = $login["img_usuario"];
	            $urlImagem = str_replace("/home/u549690031/public_html", "http://www.meussites.xyz", $urlImagem);
	            $usuario["imagemUsuario"] = $urlImagem;
	            //Atualizar código
	            header('Content-Type: bitmap; charset=utf-8');
				echo json_encode($usuario);

			} else {
				header('Content-Type: bitmap; charset=utf-8');
				echo json_encode($usuario);

			}


		}


		public function login_facebook(){

			header('Content-Type: text/html; charset=utf-8');
			$id_facebook = $this->input->post('id_facebook');

			if (!isset($id_facebook)) {
				$resultado['status'] = -1;
				$resultado['aviso']  = "Envie o ID do Facebook";
				echo json_encode($resultado);
				die();
			}

			$resultado = $this->model_webservice->loginFacebookApp($id_facebook);

			if ($resultado) {
				$resultado['status'] = 1;
				$resultado['aviso'] = "";
			} else {
				$resultado = array();
				$resultado['status'] = -1;
				$resultado['aviso']  = "Conta não localizada";
			}
			echo json_encode($resultado);

		}

		public function listar_Bancos(){
			//Usando mesma sequencia lógica do primeiro WebService
			$dados = $this->model_webservice->listarBancos();
			if ($dados) {
				$response = array();

			    foreach ($dados as $key => $row) {
			    
			        $banco = array();
			        $banco["idBanco"] = $row["idBanco"];
			        $banco["nomeBanco"] = $row["nomeBanco"];

					array_push($response, $banco);

			    }
			    header('Content-Type: bitmap; charset=utf-8');
			    echo json_encode($response);

			} else {
				header('Content-Type: bitmap; charset=utf-8');
				$response = "NENHUM BANCO ENCONTRADO";
				echo json_encode($response);

			}

		}

		public function checarUsuarioExistente(){

			$this->form_validation->set_rules('emailUsuario','E-mail do Usuário','required|is_unique[seg_usuarios.email_usuario]');

			if (!$this->form_validation->run()) {
				
				$response["success"] = 1;
				$response["message"] = "EXISTE";

			} else {

				$response["success"] = 0;
				$response["message"] = "NENHUM USUARIO ENCONTRADO";

			}
			header('Content-Type: bitmap; charset=utf-8');
			echo json_encode($response);

		}

		public function novo_Usuario(){

			if (isset($_POST['nomeUsuario']) && isset($_POST['nomeUsuarioVisualizacao']) && isset($_POST['senhaUsuario']) && isset($_POST['emailUsuario']) && isset($_POST['telefoneUsuario'])
			    && isset($_POST['bancoUsuario']) && isset($_POST['agenciaUsuario']) && isset($_POST['contaUsuario']) && isset($_POST['digitoUsuario']) && isset($_POST['cpfUsuario']) && isset($_POST['imagemUsuario']))
			{

			$dia = date("Y-m-d");
		    $hora = date("H:i:s");

		    $urlArquivo = $dia . $hora . ".png";
		    $img = $_POST['imagemUsuario'];
		    $imagemUsuario = str_replace(" ", "+", $img);
		    $binario = base64_decode($imagemUsuario);

			$dados = array (

				'login_usuario'    => $_POST['nomeUsuario'],
				'nome_usuario'     => $_POST['nomeUsuarioVisualizacao'],
				'senha_usuario'    => $_POST['senhaUsuario'],
				'email_usuario'    => $_POST['emailUsuario'],
				'telefone_usuario' => preg_replace("/[^0-9]/", "", $_POST['telefoneUsuario']),
				'banco_usuario'    => $_POST['bancoUsuario'],
				'agencia_usuario'  => $_POST['agenciaUsuario'],
				'conta_usuario'    => $_POST['contaUsuario'],
				'digito_usuario'   => $_POST['digitoUsuario'],
				'cpf_usuario'      => preg_replace("/[^0-9]/", "", $_POST['cpfUsuario']),
				'img_usuario'      => $urlArquivo,
				'ativo_usuario'    => 1,
				'fk_grupo_usuario' => 2,
				'fk_valor_usuario' => 1

			);
		   
			if($this->model_webservice->validarCpf($dados['cpf_usuario'])){
				$response["success"] = 0;
		        $response["message"] = "CPF Já cadastrado";
		        header('Content-Type: bitmap; charset=utf-8');
		    	echo json_encode($response);
			} else {

				$this->model_webservice->start();
			    $this->model_webservice->novoUsuario($dados);
			   
				$commit = $this->model_webservice->commit();

				if ($commit['status']) {
			        header('Content-Type: bitmap; charset=utf-8');
			        $urlArquivo2 = $_SERVER['DOCUMENT_ROOT'].base_url()."upload/foto_usuario/".$urlArquivo;
			        file_put_contents($urlArquivo2, $binario);
			        $response["success"] = 1;
			        $response["message"] = "CADASTRADO COM SUCESSO";

			        //Envio de E-mail de boas vindas
			        $this->email->from('megamil@megamil.net', 'Chancce | Nova Conta'); 
					$this->email->to($dados['email_usuario']); 
					$this->email->subject('Chancce | Nova Conta'); 
					//Atualizar para o modelo 
					$this->email->message('
					<html>
						<head>
						    <title>E-mail</title>
						    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
						    <style>
						        body{
						            font-family: \'Open Sans\', sans-serif;
						        }
						    </style>
						</head>
						<body>
						<p style="margin-top: 30px">&nbsp;</p>
						<p style="text-align: center"><img src="http://'.$_SERVER['HTTP_HOST'].base_url().'style/img/logoemail.png" width="130px"></p>
						<p style="margin-top: 30px">&nbsp;</p>
						<p style="width: 60%;margin: 0 auto; text-align: justify">

						<h1 style="text-align: center;font-weight: bolder;">Olá '.$_POST['nomeUsuarioVisualizacao'].'</h1>

						<p style="margin-top: 30px">&nbsp;</p>
						<p style="text-align: center;width: 320px;margin: 0 auto;font-weight: bolder;color: #696969">
						    Seja bem-vindo(a) ao Chancce, obrigado por realizar seu cadastro, fique atento para mais novidades e atualizações
						</p>

						<p style="margin-top: 30px">&nbsp;</p>
						<h3 style="text-align: center;font-weight: bolder;">
						    Dúvidas?
						</h3 style="text-align: center">
						<p style="text-align: center;margin-top: -15px;font-weight: bolder;color: #696969">Entre em contato com: <span style="color:#47A9D6"> contato@chancce.com</span></p>
						<p style="text-align: center;color:#47A9D6;font-weight: bolder;">wwww.chancce.com</p>




						</p>
						</body>
					</html>'); 

					// Enviar... 
					if ($this->email->send()) { 
						$json = array(
							'status' => 1
						);
					} else {
						$json = array(
							'resultado' => "Erro ao enviar email. <br>".$this->email->print_debugger(),
							'status' => 0
						);
					}
			        //Envio de E-mail de boas vindas

			        // echoing JSON res¬p onse
			        echo json_encode($response);
			    } else {
			        // failed to insert row
			        $response["success"] = 0;
			        $response["message"] = "Falha ao cadastrar: ".$commit['message'];

			        // echoing JSON response
			        header('Content-Type: bitmap; charset=utf-8');
			        echo json_encode($response);
			    }

			}

		} else {
			$response["success"] = 0;
	        $response["message"] = "Falha ao cadastrar, Campo(s) em branco";

	        header('Content-Type: bitmap; charset=utf-8');
		    echo json_encode($response);

		}

	} 

	public function atualizar_Usuario(){

			if (isset($_POST['nomeUsuario']) && isset($_POST['nomeUsuarioVisualizacao']) && isset($_POST['emailUsuario']) && isset($_POST['telefoneUsuario'])
			    && isset($_POST['bancoUsuario']) && isset($_POST['agenciaUsuario']) && isset($_POST['contaUsuario']) && isset($_POST['digitoUsuario']) && isset($_POST['cpfUsuario']) && isset($_POST['imagemUsuario']))
			{

			$dia = date("Y-m-d");
		    $hora = date("H:i:s");

		    $urlArquivo = $dia . $hora . ".png";
		    $img = $_POST['imagemUsuario'];
		    $imagemUsuario = str_replace(" ", "+", $img);
		    $binario = base64_decode($imagemUsuario);

			$dados = array (

				'id_usuario'       => $_POST['idUsuario'],
				'login_usuario'    => $_POST['nomeUsuario'],
				'nome_usuario'     => $_POST['nomeUsuarioVisualizacao'],
				'email_usuario'    => $_POST['emailUsuario'],
				'telefone_usuario' => preg_replace("/[^0-9]/", "", $_POST['telefoneUsuario']),
				'banco_usuario'    => $_POST['bancoUsuario'],
				'agencia_usuario'  => $_POST['agenciaUsuario'],
				'conta_usuario'    => $_POST['contaUsuario'],
				'digito_usuario'   => $_POST['digitoUsuario'],
				'cpf_usuario'      => preg_replace("/[^0-9]/", "", $_POST['cpfUsuario']),
				'img_usuario'      => $urlArquivo

			);
		   
			$this->model_webservice->start();
		    $this->model_webservice->atualizarUsuario($dados);
		   
			$commit = $this->model_webservice->commit();

			if ($commit['status']) {
		        header('Content-Type: bitmap; charset=utf-8');
		        $urlArquivo2 = $_SERVER['DOCUMENT_ROOT'].base_url()."upload/foto_usuario/".$urlArquivo;
		        file_put_contents($urlArquivo2, $binario);
		        $response["success"] = 1;
		        $response["message"] = "EDITADO COM SUCESSO ".$_POST['idUsuario'];

		        // echoing JSON res¬p onse
		        echo json_encode($response);
		    } else {
		        // failed to insert row
		        $response["success"] = 0;
		        $response["message"] = "Falha ao editar: ".$commit['message'];

		        // echoing JSON response
		        header('Content-Type: bitmap; charset=utf-8');
		        echo json_encode($response);
		    }

		} else {

			$response["success"] = 0;
	        $response["message"] = "Falha ao editar, Campo(s) em branco";

	        header('Content-Type: bitmap; charset=utf-8');
		    echo json_encode($response);

		}

	} 

	public function nova_senha(){

		$senha = array (

			'senha_usuario_atual' => $this->input->post('senhaAntiga'),
			'id_usuario' => $this->input->post('idUsuario'),
			'senha_usuario' => $this->input->post('senhaNova')

		);

		if ($senha['senha_usuario_atual'] != "" & $senha['id_usuario'] != "" && $senha['senha_usuario'] != "") {
			
			$resultado = $this->model_webservice->novaSenha($senha);

			if ($resultado) {
				
				echo json_encode("SENHA ALTERADA COM SUCESSO");
				
			} else {

				echo json_encode("ERRO AO ALTERAR SENHA");

			}

		} else {

			echo json_encode("ENVIAR TODOS OS ATRIBUTOS");

		}

		

	}

	public function recuperarSenha(){

		$email_post = $this->input->post('emailUsuario');

		if($email_post == "" || !$this->model_webservice->validarEmail($email_post)){
			
			$json = array(
				'resultado' => "ENVIAR EMAIL DO USUÁRIO",
				'status' => 0
			);

			header('Content-Type: application/json');
			echo json_encode($json);
			die();
		}

		$caracteres = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0';
		$caractereVetor = explode(',',$caracteres);
		$senha = '';

		while(strlen($senha) < 10) { //Cria uma nova senha com 10 caracteres.
			
			$indice = mt_rand(0, count($caractereVetor) - 1);
			$senha .= $caractereVetor[$indice];

		}

		$this->model_webservice->start();
		$this->model_webservice->senha_Email(sha1($senha),$this->input->post('emailUsuario'));
		$commit = $this->model_webservice->commit();

		if (!$commit['status']) {
			//echo 'Usuário está desabilitado ou não existe, entre em contato com o administrador!';
			$json = array(
				'resultado' => "Dados incorretos",
				'status' => 0
			);

		} else { //Senha enviada para o E-mail.

			// Detalhes do Email. 
			$this->email->from('megamil@megamil.net', 'CHANCCE | Troca de senha'); 
			$this->email->to($this->input->post('emailUsuario')); 
			$this->email->subject('CHANCCE | Troca de senha'); 
					$this->email->message('
				<html>
					<head>
					    <title>E-mail</title>
					    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
					    <style>
					        body{
					            font-family: \'Open Sans\', sans-serif;
					        }
					    </style>
					</head>
					<body>
					<p style="margin-top: 30px">&nbsp;</p>
					<p style="text-align: center"><img src="http://'.$_SERVER['HTTP_HOST'].base_url().'style/img/chancce_fav.png" width="130px"></p>
					<p style="margin-top: 30px">&nbsp;</p>
					<p style="width: 60%;margin: 0 auto; text-align: justify">

					<h1 style="text-align: center;font-weight: bolder;">Sua nova senha: '.$senha.'</h1>

					<p style="margin-top: 30px">&nbsp;</p>
					<p style="text-align: center;width: 320px;margin: 0 auto;font-weight: bolder;color: #696969">
					    Nova senha solicitada em: '.date('d/m/Y H:i:s').'
					</p>

					<p style="margin-top: 30px">&nbsp;</p>
					<h3 style="text-align: center;font-weight: bolder;">
					    Dúvidas?
					</h3 style="text-align: center">
					<p style="text-align: center;margin-top: -15px;font-weight: bolder;color: #696969">Entre em contato com: <span style="color:#47A9D6"> contato@chancce.com</span></p>
					<p style="text-align: center;color:#47A9D6;font-weight: bolder;">wwww.chancce.com.br</p>


					</p>
					</body>
				</html>'); 

			// Enviar... 
			if ($this->email->send()) { 
				$this->model_webservice->commit();
				$json = array(
					'resultado' => "USUARIO ATUALIZADO COM SUCESSO",
					'status' => 1
				);
			} else {
				$this->model_webservice->rollback();
				$json = array(
					'resultado' => "Erro ao enviar senha. <br>".$this->email->print_debugger(),
					'status' => 0
				);
			}

		}

		header('Content-Type: application/json');
		echo json_encode($json);

	}

	public function qr_Code(){

		$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

		if($iPod || $iPhone || $iPad){
		    header("Location: https://www.apple.com/br/ios/ios-10/");
			die();
		} else if($Android){
			header("Location: https://www.android.com/");
			die();
		} else { //Caso abra pelo Navegador de MAC / PC / Linux
			echo "Baixe Nosso App: <br>
					Android: <br>
					iOS: ";
		}

	}

	public function buscar_Canhotos(){

		$headers = array('Content-Type: application/json');
		$campos = array('D' => date('Y-m-d', strtotime('-2 days', strtotime(date('Y-m-d')))));
		$url = 'http://mobile.diaslog.com.br/api/CanhotoChancce?' . http_build_query($campos);

		$data_buscar = date('Y-m-d', strtotime('-2 days', strtotime(date('Y-m-d'))));
		$blocos = $this->model_webservice->validarData($data_buscar);
		if($blocos == 0) {

			echo "Não existem blocos desta data: {$data_buscar} Quantidade: {$blocos}";
			
			$ch = curl_init();
		
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			
			$resultado = curl_exec($ch);
			$canhotos = json_decode($resultado);

			curl_close($ch);

			if (count($canhotos) > 0) {

				$this->model_webservice->start();
				$this->model_webservice->gravarCanhotos($canhotos);
				$commit = $this->model_webservice->commit();

				if ($commit['status']) {
					echo count($canhotos).' Canhotos Inseridos.';
				} else {
					echo 'Chamando Novamente.';
					$this->buscar_Canhotos();
				}

			} else {
				echo count($canhotos).' Canhotos';
			}

			
			
			// echo '<pre>';
			// $result_arr = json_decode($resultado, true);
			// print_r($result_arr);
			// echo '</pre>';

		} else {
			
			echo "Já existem blocos desta data: {$data_buscar} Quantidade: {$blocos}";

		}

	}

	//Lista dos blocos atuais no app.
	public function listar_Blocos() {

		$id_usuario = $this->input->post('id_usuario');

		header('Content-Type: application/json');
		echo json_encode($this->model_webservice->listarBlocos($id_usuario));

	}

	//Confirmar que um usuário selecionou um bloco,
	public function selecionou_Bloco(){

		$dados = array (

			'fk_usuario' => $this->input->post('id_usuario'),
			'id_bloco' => $this->input->post('id_bloco')

		);

		$resultado = $this->model_webservice->selecionouBloco($dados);

		if($resultado){ //Sucesso
			echo json_encode(array('status' => 1, 'badge_blocos' => $resultado->badge_blocos));
		} else { // Falha
			echo json_encode(array('status' => 0));
		}

	}

	//Listar Blocos em andamento por usuário, exibindo tempo restante, remover blocos que expiraram.
	public function listar_Blocos_Usuario(){

		$usuario = $this->input->post('id_usuario');
		$blocos = $this->model_webservice->listarBlocosUsuario($usuario);
		echo json_encode($blocos);

	}

	//Exibir canhoto atual
	public function ler_Canhotos(){

		$id_bloco = $this->input->post('id_bloco');
		$canhoto = $this->model_webservice->lerCanhotos($id_bloco);
		echo json_encode($canhoto);

	}

	public function atualizar_Canhoto(){

		$dados = array (
			'opcoes' => $this->input->post('fk_opcao_checklist'),
			'ws_id_canhoto' => $this->input->post('ws_id_canhoto'),
			'id_usuario' => $this->input->post('id_usuario'),
			'id_bloco' => $this->input->post('id_bloco')		
		);

		//atualiza o canhoto e retorna o próximo canhoto
		$canhoto = $this->model_webservice->atualizarCanhoto($dados);
		echo json_encode($canhoto);
		

	}
	
	//Lista dados por bloco auditado
	public function bloco_auditado(){

		$info = $this->model_webservice->blocoAuditado($this->input->post('id_usuario'));
		echo json_encode($info);

	}

	//Lista dados por bloco pós auditado
	public function bloco_pos_auditado(){

		$info = $this->model_webservice->blocoPosAuditado($this->input->post('id_usuario'));
		echo json_encode($info);
	
	}

}

?>