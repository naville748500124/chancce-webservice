<!DOCTYPE html>
<html>
<head>
	<title>Chancce | PDF</title>
</head>
<body>

	<div style="width: 100%" align="center">
		<img src="<?php echo base_url() ?>style/img/Logochance.png" width="300px">
		<h1 align="center"> <i class="glyphicon glyphicon-list-alt"></i> Relatório Motoristas</h1>
	</div>


	<table border="1" cellspacing=0 cellpadding=2 align="center" width="100%">
		<thead align="center">
    <tr>
        <th>Filial</th>
        <th>Motorista</th>
        <th>Em</th>
        <th style="width: 60px;" align="center" class="no-filter">Corretos</th>
        <th style="width: 60px;" align="center" class="no-filter">Incorretos</th>
        <th style="width: 60px;" align="center" class="no-filter">Total</th>
    </tr>
    
    </thead>
    <tbody align="center">

     <?php
    

    foreach ($dados_iniciais as $empresa) {
        echo '<tr>';

        echo '<td>'.$empresa->nome_filial.'</td>';
        echo '<td>'.$empresa->nome_motorista.'</td>';
        echo '<td>'.$empresa->data_canhoto.'</td>';

        if (is_null($empresa->acertos)) {
            echo '<td>0</td>';
        } else {
            echo '<td>'.$empresa->acertos.'</td>';
        }

        $incorretos = ($empresa->total - $empresa->acertos);

        if (is_null($incorretos)) {
            echo '<td>0</td>';
        } else {
            echo '<td>'.$incorretos.'</td>';
        }

        if (is_null($empresa->total)) {
            echo '<td>0</td>';
        } else {
            echo '<td>'.$empresa->total.'</td>';
        }

        echo '</tr>';
    }
    
    ?>
    </tbody>
	</table>


</body>
</html>
