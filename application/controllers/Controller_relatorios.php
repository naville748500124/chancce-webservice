<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'/third_party/mpdf/mpdf.php';

class Controller_relatorios extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_relatorios');	

	}

	public function filtro_ajax(){

		$parametros = array(); //Recebe os valores com nomes certos do filtro

		$parametros['tabela']           = $this->input->get("tabela");
		$parametros['filtro_campo']     = $this->input->get("filtro_campo");
		$parametros['filtro_ordenacao'] = $this->input->get("filtro_ordenacao");
		$parametros['filtro_ordem']     = $this->input->get("filtro_ordem");
		$parametros['filtro_limite']    = $this->input->get("filtro_limite");

		$this->model_relatorios->start();

		//Carregando os campos dinamicos que irei receber via get, que são todos do filtro.
		$campos = $this->model_relatorios->listarCampos($parametros['tabela'],$parametros['filtro_campo']);

		$filtro = array();
		foreach ($campos as $key => $campo) {
			if ($campo['descricao_campo'] != "") {
				$campo_valor = $this->input->get($campo['nome_campo']);
				if (isset($campo_valor) && $campo_valor != "") {
					$campo['valor'] = $campo_valor;
					$filtro[$campo['nome_campo']] = $campo;
				}
			} 
		}

		$resultados = $this->model_relatorios->filtroAjax($parametros,$filtro);

		if ($this->model_relatorios->commit()) {
			echo '<table class="table table-bordered table-hover" align="center">
				<thead align="center">';
				
					foreach ($campos as $chave => $campo) {
						if ($campo['selecionado']) 
							echo  "<th>{$campo['descricao_campo']}: </th>";
					}

				echo '</thead>
				<tbody align="center">';	
					
					foreach ($resultados as $resultado) {

						echo "<tr>";
						foreach ($parametros['filtro_campo'] as $select_) {
								echo "<td>".$resultado[$select_]."</td>";
						}
						echo "</tr>";

					}
					
				echo '</tbody>
			</table>';

		}

	}

	public function ajax_excel(){

		$parametros = array(); //Recebe os valores com nomes certos do filtro

		$parametros['tabela']           = $this->input->post("tabela");
		$parametros['filtro_campo']     = $this->input->post("filtro_campo");
		$parametros['filtro_ordenacao'] = $this->input->post("filtro_ordenacao");
		$parametros['filtro_ordem']     = $this->input->post("filtro_ordem");
		$parametros['filtro_limite']    = $this->input->post("filtro_limite");

		$this->model_relatorios->start();

		//Carregando os campos dinamicos que irei receber via post, que são todos do filtro.
		$campos = $this->model_relatorios->listarCampos($parametros['tabela'],$parametros['filtro_campo']);

		$filtro = array();
		foreach ($campos as $key => $campo) {
			$campo_valor = $this->input->post($campo['nome_campo']);
			if (isset($campo_valor) && $campo_valor != "") {
				$campo['valor'] = $campo_valor;
				$filtro[$campo['nome_campo']] = $campo;
			}
		}

		$resultados = $this->model_relatorios->filtroAjax($parametros,$filtro);

		if ($this->model_relatorios->commit()) {
			//Define o charset
			$excel =  '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />';
			
			//Cabeçalho com título
			//Corpo com o resultado, igual ao filtro ajax.
			$excel .=  '<table border="1">
				<thead>
				<tr>
					<th colspan="'.count($parametros['filtro_campo']).'" align="center">'.$this->input->post("titulo").'</th>
				</tr>
				<tr>';
					foreach ($campos as $chave => $campo) {
						if ($campo['selecionado']) 
							$excel .=  "<th align=\"center\">{$campo['descricao_campo']}: </th>";
					}
				$excel .=  '</tr></thead>
				<tbody align="center">';	
					
					foreach ($resultados as $count => $resultado) {

						if ($count % 2) {
							$excel .=  "<tr style=\"background-color: #eee\">";
						} else {
							$excel .=  "<tr>";
						}

						foreach ($parametros['filtro_campo'] as $select_) {
								$excel .=  "<td align=\"center\">".$resultado[$select_]."</td>";
						}
						$excel .=  "</tr>";

					}

			//Formatando o texto que irá no redapé
			$label_limite = "";
			$label_ordem = "";
			$label_campos = "";
			$label_filtro = "";

			//Caso tenha um limite estipulado
			if ($this->input->post("filtro_limite") != "") {
				$label_limite = "<tr>
									<td colspan=\"".count($parametros['filtro_campo'])."\">
										<strong>Limitar a: ({$this->input->post("filtro_limite")}) resultado(s)</strong>
									</td>
								</tr>"; 
			}

			//Caso tenha uma ordenação dos campos
			if (count($this->input->post("filtro_ordenacao")) > 0) {
				$label_ordem = "<tr>
									<td colspan=\"".count($parametros['filtro_campo'])."\"> <strong>Ordenando por: </strong>";

				foreach ($this->input->post("filtro_ordenacao") as $key => $valor) {
					foreach ($campos as $key => $campo) {
						if ($campo['nome_campo'] == $valor) {
							$label_ordem .= "{$campo['descricao_campo']}";
							if ($key < count($this->input->post("filtro_ordenacao"))-1) 
								$label_ordem .= ", ";
						}
					}
					
				}
										
				$label_ordem     .= ". com ordenação {$this->input->post("filtro_ordem")}
									</td>
								</tr>"; 
			}

			//Todos os campos a serem exibidos
			if (count($this->input->post("filtro_campo")) > 0) {
				$label_campos = "<tr>
									<td colspan=\"".count($parametros['filtro_campo'])."\"><strong>Campos Exibidos: </strong>";

				foreach ($this->input->post("filtro_campo") as $key => $valor) {
					foreach ($campos as $key => $campo) {
						if ($campo['nome_campo'] == $valor) {
							$label_campos .= "{$campo['descricao_campo']}";
							if ($key < count($this->input->post("filtro_campo"))-1) 
							$label_campos .= ", ";
						}
					}
				}
										
				$label_campos     .= "</td>
								</tr>"; 
			}

			//Todos os campos a serem exibidos
			if (count($label_filtro) > 0) {

				$label_filtro .= "<tr>
										<th colspan=\"".count($parametros['filtro_campo'])."\"> Filtro(s) Solicitado(s) </th>

								  </tr>";

				foreach ($filtro as $key => $campo) {
					$possivel_campo_texto = $this->input->post($campo['nome_campo']."_texto");
					if (isset($possivel_campo_texto)) {
						$label_filtro .= "<tr>
										<td colspan=\"".count($parametros['filtro_campo'])."\"> <strong>{$campo['descricao_campo']}</strong> : {$possivel_campo_texto}.</td>
									  </tr>";
					} else {
						$label_filtro .= "<tr>
										<td colspan=\"".count($parametros['filtro_campo'])."\"> <strong>{$campo['descricao_campo']}</strong> : {$campo['valor']}.</td>
									  </tr>";
					}
					
				}
										
			}
			

				//Footer com dados da exportação e filtro usado etc.
			$excel .= "<table>
						<tbody>
						<tr></tr>
						<tr></tr>
						{$label_filtro}
						<tr></tr>
						{$label_campos}
						{$label_ordem}
						{$label_limite}
							
						<tr></tr>

						<tr>
							<td colspan=\"".count($parametros['filtro_campo'])."\">
								<strong>DATA DA EXPORTAÇÃO: ".date('d/m/Y H:i:s')."</strong>
							</td>
						</tr>
						<tr>
							<td colspan=\"".count($parametros['filtro_campo'])."\">
								Solicitado por: ".$this->session->userdata('nome')."
							</td>
						</tr>
						<tr>
							<td colspan=\"".count($parametros['filtro_campo'])."\">
								Megamil.net
							</td> 
						</tr>
					</tbody>
				</table>";


		}

		$arquivo = 'excel_relatorio_'.$this->input->post('tabela').'_'.date('d-m-Y-H:i:s').'.xls';

		// Configurações header para forçar o download
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
		header ("Content-Transfer-Encoding: binary"); 
		header ("Content-Type: application/vnd.ms-excel"); 
		header ("Expires: 0"); 
		header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
		header ("Content-Description: PHP Generated Data");

		// Envia o conteúdo do arquivo
		chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
		echo $excel;
		exit;

	}

	public function load_historico_edicoes(){
		$id_aplicacao = $this->input->post("id");

		$dados = $this->model_relatorios->loadHistoricoEdicoes($id_aplicacao);

		echo '<table class="table table-bordered table-hover">
				<thead>
				<tr>
					<th>Alterado Por</th>
					<th>Alterado Em:</th>
					<th>Campo Alterado</th>
					<th>De</th>
					<th>Para</th>
				</tr>
				<tbody>';

		foreach ($dados as $key => $value) {
			echo "<tr>";
			echo "<td>".$value->nome_usuario."</td>";
			echo "<td>".$value->data_log_formatado."</td>";
			echo "<td>".$value->campo."</td>";
			echo "<td>".$value->original_edicao."</td>";
			echo "<td>".$value->novo_edicao."</td>";
			echo "</tr>";
		}

		echo '</tbody>
		</table>';

	}

	public function excel_lista() {

			$empresas = $this->model_relatorios->view_relatorio();

			$excel = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
				<table class="table table-bordered table-hover" align="center">
					    <thead align="center">
					    <tr>
					        <th>Filial</th>
					        <th>Motorista</th>
					        <th style="width: 60px;" align="center" class="no-filter">Corretos</th>
					        <th style="width: 60px;" align="center" class="no-filter">Incorretos</th>
					        <th style="width: 60px;" align="center" class="no-filter">Total</th>
					    </tr>
					    
					    </thead>
					    <tbody align="center">';

					    foreach ($empresas as $empresa) {
					        $excel .= '<tr>';

					        $excel .= '<td>'.$empresa->nome_filial.'</td>';
					        $excel .= '<td>'.$empresa->nome_motorista.'</td>';

					        if (is_null($empresa->acertos)) {
					            $excel .= '<td>0</td>';
					        } else {
					            $excel .= '<td>'.$empresa->acertos.'</td>';
					        }

					        $incorretos = ($empresa->total - $empresa->acertos);

					        if (is_null($incorretos)) {
					            $excel .= '<td>0</td>';
					        } else {
					            $excel .= '<td>'.$incorretos.'</td>';
					        }

					        if (is_null($empresa->total)) {
					            $excel .= '<td>0</td>';
					        } else {
					            $excel .= '<td>'.$empresa->total.'</td>';
					        }

					        $excel .= '</tr>';
					    }
					    
					    $excel .= '</tbody>
					</table>';


			$arquivo = 'excel_relatorio.xls';

			// Configurações header para forçar o download
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header ("Content-Transfer-Encoding: binary"); 
			header ("Content-Type: application/vnd.ms-excel"); 
			header ("Expires: 0"); 
			header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
			header ("Content-Description: PHP Generated Data");

			// Envia o conteúdo do arquivo
			chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
			echo $excel;
			exit;

		}

	public function pdf_relatorios(){

			$dados['relatorio'] = $this->model_relatorios->view_relatorio();

			if (true) {//Gerar PDF
				// Instancia a classe mPDF
				$mpdf = new mPDF();
				// Ao invés de imprimir a view 'welcome_message' na tela, passa o código
				// HTML dela para a variável $html
				$html = $this->load->view('relatorio/pdf_relatorios',$dados,true);
				// Define um Cabeçalho para o arquivo PDF
				//$mpdf->SetHeader('Megamil OS');
				// Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
				// página através da pseudo-variável PAGENO
				$mpdf->SetFooter('<div style="width: 100%">Chancce | '.date('d/m/Y H:i:s').' Pag. {PAGENO} </div>');
				// Insere o conteúdo da variável $html no arquivo PDF
				$mpdf->writeHTML($html);
				// Cria uma nova página no arquivo
				//$mpdf->AddPage();
				// Insere o conteúdo na nova página do arquivo PDF
				//$mpdf->WriteHTML('<p><b>Minha nova página no arquivo PDF</b></p>');
				// Gera o arquivo PDF
				$mpdf->Output();
			} else {
				$this->load->view('relatorio/pdf_relatorios',$dados);
			}
		
		}

	/*Ajax FIM*/

	public function data($data = null,$timestamp = null){

		if ($data != "" && $timestamp) {

			return date("Y-m-d H:i:s",strtotime(str_replace('/','-',$data)));
			
		} else if($data != "" && !$timestamp) {
			return date("Y-m-d",strtotime(str_replace('/','-',$data)));
		} else {
			return 0;
		}

	}


}