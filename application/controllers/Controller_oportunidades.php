<?php defined('BASEPATH') OR exit('No direct script access allowed');

	include_once APPPATH.'/third_party/mpdf/mpdf.php';

	class Controller_oportunidades extends CI_Controller {

		function __construct() {

		    parent::__construct();
		    $this->load->model('model_oportunidades');
			    
		}
		
		public function excel_lista() {

			$blocos = $this->model_oportunidades->view_oportunidades();

			$excel = '<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
				<table class="table table-bordered table-hover" align="center">
				<thead align="center">
					<th>Usuário</th>
					<th>Data</th>
					<th>Bloco</th>
					<th>Canhotos</th>
					<th>Valor</th>
					<th>Status</th>
				</thead>
				<tbody align="center">';

					foreach ($blocos as $bloco) {
						$excel .= '<tr>';

						$excel .= '<td>'.$bloco->fk_usuario.'</td>';
						$excel .= '<td>'.$bloco->data_bloco.' as '.$bloco->data_bloco_usuario.'</td>';
						$excel .= '<td>'.$bloco->bloco.'</td>';
						$excel .= '<td>'.$bloco->qtd_canhotos.'</td>';
						$excel .= '<td>'.($bloco->valor_canhoto * $bloco->qtd_canhotos).'</td>';
						$excel .= '<td>Auditados:'.$bloco->auditado1.' <br> Pós Auditados: '.$bloco->auditado2.'</td>';

						$excel .= '</tr>';
					}

				$excel .= '</tbody>
			</table>';


			$arquivo = 'excel_blocos.xls';

			// Configurações header para forçar o download
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
			header ("Content-Transfer-Encoding: binary"); 
			header ("Content-Type: application/vnd.ms-excel"); 
			header ("Expires: 0"); 
			header ("Content-Disposition: attachment; filename=\"{$arquivo}\"");
			header ("Content-Description: PHP Generated Data");

			// Envia o conteúdo do arquivo
			chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $excel); 
			echo $excel;
			exit;

		}


		public function pdf_oportunidade(){

			$dados['blocos'] = $this->model_oportunidades->view_oportunidades();

			if (true) {//Gerar PDF
				// Instancia a classe mPDF
				$mpdf = new mPDF();
				// Ao invés de imprimir a view 'welcome_message' na tela, passa o código
				// HTML dela para a variável $html
				$html = $this->load->view('oportunidades/pdf_oportunidade',$dados,true);
				// Define um Cabeçalho para o arquivo PDF
				//$mpdf->SetHeader('Megamil OS');
				// Define um rodapé para o arquivo PDF, nesse caso inserindo o número da
				// página através da pseudo-variável PAGENO
				$mpdf->SetFooter('<div style="width: 100%">Chancce | '.date('d/m/Y H:i:s').' Pag. {PAGENO} </div>');
				// Insere o conteúdo da variável $html no arquivo PDF
				$mpdf->writeHTML($html);
				// Cria uma nova página no arquivo
				//$mpdf->AddPage();
				// Insere o conteúdo na nova página do arquivo PDF
				//$mpdf->WriteHTML('<p><b>Minha nova página no arquivo PDF</b></p>');
				// Gera o arquivo PDF
				$mpdf->Output();
			} else {
				$this->load->view('oportunidades/pdf_oportunidade',$dados);
			}
		
		}

	
	}

?>