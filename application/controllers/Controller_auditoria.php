<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_auditoria extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_auditoria');
		    
	}

	public function atualizar_Canhoto(){

		$this->model_auditoria->start();

		if(!isset($_POST['canhoto'])){

			$this->aviso('Selecione','Nenhum Canhoto Selecionado','error',false);
			redirect('main/redirecionar/13/');

		} else {

			foreach ($_POST['canhoto'] as $key => $canhoto) {
				$checklist = $this->input->post('checklist');
				//Nenhum Erro
				if(!isset($checklist)){

					$this->model_auditoria->atualizarCanhoto($canhoto,null);

				} else {
						
					$this->model_auditoria->atualizarCanhoto($canhoto,$this->input->post('checklist'));

				}

			}

			$commit = $this->model_auditoria->commit();

			if ($commit['status']) {
				$this->aviso('Canhotos Pós Auditados','Canhoto(s) pós auditado(s) com sucesso','success',false);

				redirect('main/redirecionar/13/');

			} else {

				$this->aviso('Falha ao atualizar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);
				$this->session->set_flashdata($dados);

				redirect('main/redirecionar/13');

			}

		}

	}

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}

}