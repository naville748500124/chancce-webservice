<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_empresa extends CI_Controller {

	function __construct() {

	    parent::__construct();
	    $this->load->model('model_empresas');
		    
	}

	public function criar_empresa(){

		$dados = array (

			'razao_social_empresa' => $this->input->post('razao_social_empresa'),
			'nome_fantasia_empresa' => $this->input->post('nome_fantasia_empresa'),
			'cnpj_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('cnpj_empresa')),
			'inscricao_estadual_empresa' => $this->input->post('inscricao_estadual_empresa'),
			'uf_empresa' => $this->input->post('uf_empresa'),
			'cep_empresa' => $this->input->post('cep_empresa'),
			'cidade_empresa' => $this->input->post('cidade_empresa'),
			'bairro_empresa' => $this->input->post('bairro_empresa'),
			'rua_empresa' => $this->input->post('rua_empresa'),
			'numero_empresa' => $this->input->post('numero_empresa'),
			'complemento_empresa' => $this->input->post('complemento_empresa'),
			'telefone_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('telefone_empresa')),
			'ramal_empresa' => $this->input->post('ramal_empresa'),
			'fax_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('fax_empresa')),
			'homePage_empresa' => $this->input->post('homePage_empresa'),
			'email_empresa' => $this->input->post('email_empresa'),
			'atividade1_empresa' => $this->input->post('atividade1_empresa'),
			'atividade2_empresa' => $this->input->post('atividade2_empresa'),
			'atividade3_empresa' => $this->input->post('atividade3_empresa'),

			'nome_responsavel_empresa' => $this->input->post('nome_responsavel_empresa'),
			'cel_responsavel_empresa' => $this->input->post('cel_responsavel_empresa'),
			'cpf_responsavel_empresa' => $this->input->post('cpf_responsavel_empresa'),
			
			'status_empresa' => $this->input->post('status_empresa')

		);

		$this->model_empresas->start();
		$id = $this->model_empresas->create($dados);
		$commit = $this->model_empresas->commit();
		
		if ($commit['status']) {
			
			$this->aviso('Registro Criado','Empresa criada com sucesso!','success',false);

			redirect('main/redirecionar/6/'.$id);

		} else {

			$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/5');

		}

	}

	public function editar_empresa(){

		$dados = array (

			'id_empresa' => $this->input->post('id_empresa'),
			'razao_social_empresa' => $this->input->post('razao_social_empresa'),
			'nome_fantasia_empresa' => $this->input->post('nome_fantasia_empresa'),
			'cnpj_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('cnpj_empresa')),
			'inscricao_estadual_empresa' => $this->input->post('inscricao_estadual_empresa'),
			'uf_empresa' => $this->input->post('uf_empresa'),
			'cep_empresa' => $this->input->post('cep_empresa'),
			'cidade_empresa' => $this->input->post('cidade_empresa'),
			'bairro_empresa' => $this->input->post('bairro_empresa'),
			'rua_empresa' => $this->input->post('rua_empresa'),
			'numero_empresa' => $this->input->post('numero_empresa'),
			'complemento_empresa' => $this->input->post('complemento_empresa'),
			'telefone_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('telefone_empresa')),
			'ramal_empresa' => $this->input->post('ramal_empresa'),
			'fax_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('fax_empresa')),
			'homePage_empresa' => $this->input->post('homePage_empresa'),
			'email_empresa' => $this->input->post('email_empresa'),
			'atividade1_empresa' => $this->input->post('atividade1_empresa'),
			'atividade2_empresa' => $this->input->post('atividade2_empresa'),
			'atividade3_empresa' => $this->input->post('atividade3_empresa'),

			'nome_responsavel_empresa' => $this->input->post('nome_responsavel_empresa'),
			'cel_responsavel_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('cel_responsavel_empresa')),
			'cpf_responsavel_empresa' => preg_replace("/[^0-9]/", "", $this->input->post('cpf_responsavel_empresa')),

			'status_empresa' => $this->input->post('status_empresa')

		);

		$this->model_empresas->start();
		$this->model_empresas->update($dados);
		$commit = $this->model_empresas->commit();
		
		if ($commit['status']) {
			
			$this->aviso('Registro Criado','Empresa editada com sucesso!','success',false);

			redirect('main/redirecionar/6/'.$this->input->post('id_empresa'));

		} else {

			$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/6/'.$this->input->post('id_empresa'));

		}

	}
	public function cad_filial(){

		$dados = array(
			'fk_empresa' => $this->input->post('fk_empresa'),
			'nome_filial' => $this->input->post('nome_filial'),
			'sigla_filial' => $this->input->post('sigla_filial')
		);

		$this->model_empresas->start();
		$this->model_empresas->cadFilial($dados);
		$commit = $this->model_empresas->commit();
		
		if ($commit['status']) {

			$this->aviso('Registro Criado','Filial criada com sucesso!','success',false);

			redirect('main/redirecionar/6/'.$this->input->post('fk_empresa'));

		} else {

			$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/6/'.$this->input->post('fk_empresa'));

		}

	}

	public function ajax_Editar_Filial(){

		$dados = array(
			'id_filial' => $this->input->post('id_filial'),
			'nome_filial' => $this->input->post('nome_filial'),
			'sigla_filial' => $this->input->post('sigla_filial')
		);

		$this->model_empresas->start();
		$this->model_empresas->edtFilial($dados);
		$commit = $this->model_empresas->commit();
		
		if ($commit['status']) {

			echo 'sucesso';

		} else {

			echo 'Falha ao editar filial';

		}

	}

	public function novo_motorista(){

		$dados = array(
			'fk_filial' => $this->input->post('fk_filial'),
			'id_ws_motorista' => $this->input->post('id_ws_motorista'),
			'nome_motorista' => $this->input->post('nome_motorista'),
			'endereco_motorista' => $this->input->post('endereco_motorista')
		);

		$this->model_empresas->start();
		$this->model_empresas->cadMotorista($dados);
		$commit = $this->model_empresas->commit();
		
		if ($commit['status']) {

			$this->aviso('Registro Criado','Motorista cadastrado com sucesso!','success',false);

			redirect('main/redirecionar/6/'.$this->input->post('fk_empresa'));

		} else {

			$this->aviso('Falha ao criar','Erro(s) ao inserir dados: "'.$commit['message'].'" <br> <a href="#" id="erro_feedback" cod="'.$commit['log_erro'].'">Clique Aqui Para Reportar</a>','error',true);

			$this->session->set_flashdata($dados);
			redirect('main/redirecionar/6/'.$this->input->post('fk_empresa'));

		}

	}

	public function ajax_Editar_Motorista(){

		$dados = array(
			'id_motorista' 		 => $this->input->post('id_motorista'),
			'fk_filial' 		 => $this->input->post('fk_filial'),
			'id_ws_motorista' 	 => $this->input->post('id_ws_motorista'),
			'nome_motorista' 	 => $this->input->post('nome_motorista'),
			'endereco_motorista' => $this->input->post('endereco_motorista')
		);

		$this->model_empresas->start();
		$this->model_empresas->editMotorista($dados);
		$commit = $this->model_empresas->commit();
		
		if ($commit['status']) {

			echo 'sucesso';

		} else {

			echo 'Falha ao editar filial';

		}

	}

	public function aviso($titulo,$aviso,$tipo,$fixo){

		//Toast apresenta erro quando existe uma quebra de linha, que ocorre com o validation_errors().
			$aviso_ = str_replace('
', '', $aviso);

		$aviso = str_replace('\'', '"', $aviso_);

		$this->session->set_flashdata('titulo_alerta',$titulo);
		$this->session->set_flashdata('mensagem_alerta',$aviso);
		$this->session->set_flashdata('tipo_alerta',$tipo);
		$this->session->set_flashdata('mensagem_fixa',$fixo);

	}


}