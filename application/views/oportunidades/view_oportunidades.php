<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Relatório Blocos Gerados</h1>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<tr>
			<th>Usuário</th>
			<th>Data</th>
			<th>Bloco</th>
			<th>Tempo Para realizar</th>
			<th>Canhotos</th>
			<th>Valor</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody align="center">	

	<?php 

		foreach ($dados_iniciais as $bloco) {
			echo '<tr>';

			//Calcula o tempo de upload
			$entrada = $bloco->data_bloco_usuario;
			$saida = $bloco->data_fim_bloco_usuario;

			if(!is_null($entrada) && !is_null($saida) && $saida != "" && $entrada != ""){
				$hora1 = explode(":",$entrada);
				$hora2 = explode(":",$saida);
				$acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
				$acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
				$resultado = $acumulador2 - $acumulador1;
				$hora_ponto = floor($resultado / 3600);
				$resultado = $resultado - ($hora_ponto * 3600);
				$min_ponto = floor($resultado / 60);
				$resultado = $resultado - ($min_ponto * 60);
				$secs_ponto = $resultado;
				//Grava na variável resultado final

				if ($hora_ponto < 10) { $hora_ponto = '0'.$hora_ponto; }
				if ($min_ponto < 10) { $min_ponto = '0'.$min_ponto; }
				if ($secs_ponto < 10) { $secs_ponto = '0'.$secs_ponto; }

				$tempo = $hora_ponto.":".$min_ponto.":".$secs_ponto;
			} else {
				$tempo = '-';
			}
			

			echo '<td>'.$bloco->fk_usuario.'</td>';
			echo '<td>'.$bloco->data_bloco.' as '.$bloco->data_bloco_usuario.'</td>';
			echo '<td>'.$bloco->bloco.'</td>';
			echo '<td>'.$tempo.'</td>';
			echo '<td>'.$bloco->qtd_canhotos.'</td>';
			echo '<td>'.($bloco->valor_canhoto * $bloco->qtd_canhotos).'</td>';
			echo '<td>Auditados:'.$bloco->auditado1.' <br> Pós Auditados: '.$bloco->auditado2.'</td>';

			echo '</tr>';
		}

	?>

	</tbody>
</table>

<div class="row" style="margin-left: -30px;">

	<div class="col-md-1" align="center">
		 <a id="excel" target="_blank" style="cursor: pointer;">
	      <img src="<?php echo base_url() ?>style/img/excel.jpg">
	    </a>
	</div>
	<div class="col-md-1" style="margin-left: -20px;" style="cursor: pointer;" align="left">
		<a href="<?php echo base_url() ?>Controller_oportunidades/pdf_oportunidade">
			<img src="<?php echo base_url() ?>style/img/pdf.jpg">
		</a>
	</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		//Trava para trazer os primeiros 100 resultados
		$('select[name=DataTables_Table_0_length]').val(100).trigger('change');

		$('#excel').click(function(){

	      	var url = "<?php echo base_url(); ?>Controller_oportunidades/excel_lista";
	      	$.ajax({
	        	url: url,
	         	type: "post",
	        	datatype: 'json',
	        	success: function(data){
	        	
	        		window.location = url;

	        	},
	          
	          	error:function(){
	              
	          }   
	        });

	    });

	});
</script>