<!DOCTYPE html>
<html>
<head>
	<title>Chancce | PDF</title>
</head>
<body>

	<div style="width: 100%" align="center">
		<img src="<?php echo base_url() ?>style/img/Logochance.png" width="300px">
		<h1 align="center"> <i class="glyphicon glyphicon-list-alt"></i> Relatório Blocos Gerados</h1>
	</div>


	<table border="1" cellspacing=0 cellpadding=2 align="center" width="100%">
		<thead>
			<tr>
				<th align="center">Usuário</th>
				<th align="center">Data</th>
				<th align="center">Bloco</th>
				<th align="center">Canhotos</th>
				<th align="center">Valor</th>
				<th align="center">Status</th>
			</tr>
		</thead>
		<tbody align="center">	
		<?php 
			foreach ($blocos as $grupo) {
				echo '<tr>';
					echo '<td align="center">'.$grupo->fk_usuario.'</td>';
					echo '<td align="center">'.$grupo->data_bloco.' as '.$grupo->data_bloco_usuario.'</td>';
					echo '<td align="center">'.$grupo->bloco.'</td>';
					echo '<td align="center">'.$grupo->qtd_canhotos.'</td>';
					echo '<td align="center">'.($grupo->valor_canhoto * $grupo->qtd_canhotos).'</td>';
					echo '<td align="center">Auditados:'.$grupo->auditado1.' <br> Pós Auditados: '.$grupo->auditado2.'</td>';
				echo '</tr>';
			}
		?>
		</tbody>
	</table>


</body>
</html>
