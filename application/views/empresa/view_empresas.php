<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Empresas</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/5">
			<i class="glyphicon glyphicon-plus-sign"></i> Nova Empresa
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th style="width: 60px;" align="center" class="no-filter">Editar</th>
		<th>Nome Fantasia</th>
		<th>E-mail</th>
		<th>CNPJ</th>
		<th>Status</th>
	</thead>
	<tbody align="center">	
	<?php 


		foreach ($dados_iniciais as $empresa) {
			echo '<tr>';

			echo '<td style="width: 60px;">
					<a class="btn btn-success" href="'.base_url().'main/redirecionar/6/'.$empresa->id_empresa.'"> <i class="glyphicon glyphicon-edit"> </i> Editar
					</button>
				</td>';

	
			echo '<td>'.$empresa->nome_fantasia_empresa.'</td>';
			echo '<td>'.$empresa->email_empresa.'</td>';
			echo '<td class="mascara_cnpj">'.$empresa->cnpj_empresa.'</td>';
			if ($empresa->status_empresa) {
				echo '<td>Ativo</td>';
			} else {
				echo '<td>Inativo</td>';
			}

			echo '</tr>';
		}

	?>
	</tbody>
</table>
