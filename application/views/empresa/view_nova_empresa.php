<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Nova Empresa</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>

<?php echo form_open('controller_empresa/criar_empresa'); ?>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="razao_social_empresa">Razão Social</label> 
			<input type="text" class="form-control obrigatorio" id="razao_social_empresa" name="razao_social_empresa" placeholder="Razão Social" aviso="Razão Social" value="<?php echo $this->session->flashdata('razao_social_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_fantasia_empresa">Nome Fantasia</label> 
			<input type="text" class="form-control obrigatorio" id="nome_fantasia_empresa" name="nome_fantasia_empresa" placeholder="Nome Fantasia" aviso="Nome Fantasia" value="<?php echo $this->session->flashdata('nome_fantasia_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cnpj_empresa">CNPJ</label> 
			<input type="text" class="form-control mascara_cnpj validar_cnpj obrigatorio" id="cnpj_empresa" name="cnpj_empresa" placeholder="CNPJ" aviso="CNPJ" value="<?php echo $this->session->flashdata('cnpj_empresa'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="inscricao_estadual_empresa">Inscrição Estadual</label> 
			<input type="text" class="form-control obrigatorio" id="inscricao_estadual_empresa" name="inscricao_estadual_empresa" placeholder="Inscrição Estadual" aviso="Inscrição Estadual" value="<?php echo $this->session->flashdata('inscricao_estadual_empresa'); ?>" maxlength="100">
		</div>
	</div>
</div>

<div class="row">

	<div class="col-md-1">
		<label class="control-label" for="uf_empresa">UF</label> 
		<select class="form-control" id="uf_empresa" name="uf_empresa">
			<option>SP</option>
			<option>DF</option>
			<option>Adicionar...</option>
		</select>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep_empresa">CEP</label> 
			<input type="text" class="form-control mascara_cep obrigatorio" id="cep_empresa" name="cep_empresa" placeholder="CEP" aviso="CEP" value="<?php echo $this->session->flashdata('cep_empresa'); ?>">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="cidade_empresa">Cidade</label> 
			<input type="text" class="form-control obrigatorio" id="cidade_empresa" name="cidade_empresa" placeholder="Cidade" aviso="Cidade" value="<?php echo $this->session->flashdata('cidade_empresa'); ?>" maxlength="20">
		</div>
	</div>

	<div class="col-md-5">
		<div class="form-group has-feedback">
			<label class="control-label" for="bairro_empresa">Bairro</label> 
			<input type="text" class="form-control obrigatorio" id="bairro_empresa" name="bairro_empresa" placeholder="Bairro" aviso="Bairro" value="<?php echo $this->session->flashdata('bairro_empresa'); ?>" maxlength="20">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-7">
		<div class="form-group has-feedback">
			<label class="control-label" for="rua_empresa">Rua</label> 
			<input type="text" class="form-control obrigatorio" id="rua_empresa" name="rua_empresa" placeholder="Rua" aviso="Rua" value="<?php echo $this->session->flashdata('rua_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-2">
		<label class="control-label" for="numero_empresa">Número</label> 
		<input type="text" class="form-control" id="numero_empresa" name="numero_empresa" placeholder="Número" aviso="Número" value="<?php echo $this->session->flashdata('numero_empresa'); ?>" maxlength="10">
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="complemento_empresa">Complemento</label> 
			<input type="text" class="form-control" id="complemento_empresa" name="complemento_empresa" placeholder="Complemento" aviso="Complemento" value="<?php echo $this->session->flashdata('complemento_empresa'); ?>" maxlength="100">
		</div>
	</div>

</div>


<div class="row">

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_empresa">Telefone</label> 
			<input type="text" class="form-control mascara_cel obrigatorio" id="telefone_empresa" name="telefone_empresa" placeholder="Telefone" aviso="Telefone" value="<?php echo $this->session->flashdata('telefone_empresa'); ?>">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="ramal_empresa">Ramal</label> 
			<input type="text" class="form-control validar_numeros" id="ramal_empresa" name="ramal_empresa" placeholder="Ramal" aviso="Ramal" value="<?php echo $this->session->flashdata('ramal_empresa'); ?>" maxlength="10">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="fax_empresa">Fax</label> 
			<input type="text" class="form-control mascara_cel" id="fax_empresa" name="fax_empresa" placeholder="Fax" aviso="Fax" value="<?php echo $this->session->flashdata('fax_empresa'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="homePage_empresa">Site</label> 
			<input type="text" class="form-control" id="homePage_empresa" name="homePage_empresa" placeholder="Site" aviso="Site" value="<?php echo $this->session->flashdata('homePage_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_empresa">E-mail</label> 
			<input type="text" class="form-control validar_email obrigatorio" id="email_empresa" name="email_empresa" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_empresa'); ?>" maxlength="100">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_responsavel_empresa">Nome Responsável</label> 
			<input type="text" class="form-control " id="nome_responsavel_empresa" name="nome_responsavel_empresa" placeholder="Nome Responsável" aviso="Nome Responsável" value="<?php echo $this->session->flashdata('nome_responsavel_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cel_responsavel_empresa">Telefone Responsável</label> 
			<input type="text" class="form-control mascara_cel" id="cel_responsavel_empresa" name="cel_responsavel_empresa" placeholder="Telefone Responsável" aviso="Telefone Responsável" value="<?php echo $this->session->flashdata('cel_responsavel_empresa'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cpf_responsavel_empresa">CPF Responsável</label> 
			<input type="text" class="form-control mascara_cpf validar_cpf" id="cpf_responsavel_empresa" name="cpf_responsavel_empresa" placeholder="CPF Responsável" aviso="CPF Responsável" value="<?php echo $this->session->flashdata('cpf_responsavel_empresa'); ?>">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="atividade1_empresa">Atividade 1</label> 
			<input type="text" class="form-control obrigatorio" id="atividade1_empresa" name="atividade1_empresa" placeholder="Atividade 1" aviso="Atividade 1" value="<?php echo $this->session->flashdata('atividade1_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="atividade2_empresa">Atividade 2</label> 
			<input type="text" class="form-control" id="atividade2_empresa" name="atividade2_empresa" placeholder="Atividade 2" aviso="Atividade 2" value="<?php echo $this->session->flashdata('atividade2_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="atividade3_empresa">Atividade 3</label> 
			<input type="text" class="form-control" id="atividade3_empresa" name="atividade3_empresa" placeholder="Atividade 3" aviso="Atividade 3" value="<?php echo $this->session->flashdata('atividade3_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<label class="control-label" for="status_empresa">Status</label> 
		<select class="form-control obrigatorio" id="status_empresa" name="status_empresa">
			<option value="1">Ativa</option>
			<option value="0">Inativa</option>
		</select>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-floppy-disk"></i> Criar </button>
	</div>
</div>

<?php echo form_close(); ?>