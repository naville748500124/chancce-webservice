<style type="text/css">
	.btn {
		color: white;
		text-shadow: 1px 1px 1px black;
	}
</style>

<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-file"></i> Editar Empresa</h1>
	</div>
	<div class="col-md-4" align="right">
		<button type="button" class="btn btn-info" id="voltar"> <i class="glyphicon glyphicon-backward"></i> Voltar</button>
		<button type="button" class="btn btn-info" id="recarregar" url="<?php echo $_SERVER ['REQUEST_URI'] ?>"> <i class="glyphicon glyphicon-refresh"></i> Recarregar</button>
	</div>
</div>
<hr>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Nova Filial</h4>
      </div>
      <div class="modal-body">
      	<?php echo form_open('controller_empresa/cad_filial'); ?>

      	<input type="hidden" name="fk_empresa" value="<?php echo $this->session->flashdata('id_empresa_edicao'); ?>">

      	<div class="row">
      		<div class="col-md-4">
      			<label>Nome</label>
      			<input class="form-control" type="text" name="nome_filial" placeholder="Nome Filial">
      		</div>
      		<div class="col-md-4">
      			<label>Sigla</label>
      			<input class="form-control" type="text" name="sigla_filial" placeholder="Sigla Filial" maxlength="3">
      		</div>
      	</div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success">Cadastrar</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalMotorista" tabindex="-1" role="dialog" aria-labelledby="myModalMotoristaLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalMotoristaLabel">Novo Motorista</h4>
      </div>
      <div class="modal-body">
      	<?php echo form_open('controller_empresa/novo_motorista'); ?>

      	<input type="hidden" name="fk_empresa" value="<?php echo $this->session->flashdata('id_empresa_edicao'); ?>">

      	<div class="row">
      		<div class="col-md-3">
      			<label>ID Webservice</label>
      			<input class="form-control validar_numeros" type="text" name="id_ws_motorista" placeholder="ID Webservice">
      		</div>
      		<div class="col-md-3">
      			<label>Nome</label>
      			<input class="form-control" type="text" name="nome_motorista" placeholder="Nome Motorista">
      		</div>
      		<div class="col-md-3">
      			<label>Endereço</label>
      			<input class="form-control" type="text" name="endereco_motorista" placeholder="Endereço">
      		</div>
      		<div class="col-md-3">
      			<label>Filial</label>
      			<select name="fk_filial">
      				<?php foreach ($dados_iniciais['filiais'] as $filial) {

      					echo '<option value="'.$filial->id_filial.'">'.$filial->nome_filial.'</option>';

      				} ?>
      			</select>
      		</div>
      	</div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success">Cadastrar</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalMotoristaEditar" tabindex="-1" role="dialog" aria-labelledby="myModalMotoristaEditarLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalMotoristaEditarLabel">Editar Motorista</h4>
      </div>
      <div class="modal-body">

      	<input type="hidden" id="id_motorista">

      	<div class="row">
      		<div class="col-md-3">
      			<label>ID Webservice</label>
      			<input class="form-control validar_numeros" type="text" id="id_ws_motorista" placeholder="ID Webservice">
      		</div>
      		<div class="col-md-3">
      			<label>Nome</label>
      			<input class="form-control" type="text" id="nome_motorista" placeholder="Nome Empresa">
      		</div>
      		<div class="col-md-3">
      			<label>Endereço</label>
      			<input class="form-control" type="text" id="endereco_motorista" placeholder="Endereço">
      		</div>
      		<div class="col-md-3">
      			<label>Filial</label>
      			<select id="fk_filial">
      				<?php foreach ($dados_iniciais['filiais'] as $filial) {

      					echo '<option value="'.$filial->id_filial.'">'.$filial->nome_filial.'</option>';

      				} ?>
      			</select>
      		</div>
      	</div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-success" id="edtMotorista">Editar</button>
      </div>
    </div>
  </div>
</div>

<?php echo form_open('controller_empresa/editar_empresa'); ?>

<input type="hidden" name="id_empresa" value="<?php echo $this->session->flashdata('id_empresa_edicao'); ?>">

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="razao_social_empresa">Razão Social</label> 
			<input type="text" class="form-control obrigatorio" id="razao_social_empresa" name="razao_social_empresa" placeholder="Razão Social" aviso="Razão Social" value="<?php echo $this->session->flashdata('razao_social_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_fantasia_empresa">Nome Fantasia</label> 
			<input type="text" class="form-control obrigatorio" id="nome_fantasia_empresa" name="nome_fantasia_empresa" placeholder="Nome Fantasia" aviso="Nome Fantasia" value="<?php echo $this->session->flashdata('nome_fantasia_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cnpj_empresa">CNPJ</label> 
			<input type="text" class="form-control mascara_cnpj validar_cnpj obrigatorio" id="cnpj_empresa" name="cnpj_empresa" placeholder="CNPJ" aviso="CNPJ" value="<?php echo $this->session->flashdata('cnpj_empresa_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="inscricao_estadual_empresa">Inscrição Estadual</label> 
			<input type="text" class="form-control obrigatorio" id="inscricao_estadual_empresa" name="inscricao_estadual_empresa" placeholder="Inscrição Estadual" aviso="Inscrição Estadual" value="<?php echo $this->session->flashdata('inscricao_estadual_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>
</div>

<div class="row">

	<div class="col-md-1">
		<label class="control-label" for="uf_empresa">UF</label> 
		<select class="form-control" id="uf_empresa" name="uf_empresa">
			<option>SP</option>
			<option>DF</option>
			<option>Adicionar...</option>
		</select>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="cep_empresa">CEP</label> 
			<input type="text" class="form-control mascara_cep obrigatorio" id="cep_empresa" name="cep_empresa" placeholder="CEP" aviso="CEP" value="<?php echo $this->session->flashdata('cep_empresa_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-4">
		<div class="form-group has-feedback">
			<label class="control-label" for="cidade_empresa">Cidade</label> 
			<input type="text" class="form-control obrigatorio" id="cidade_empresa" name="cidade_empresa" placeholder="Cidade" aviso="Cidade" value="<?php echo $this->session->flashdata('cidade_empresa_edicao'); ?>" maxlength="20">
		</div>
	</div>

	<div class="col-md-5">
		<div class="form-group has-feedback">
			<label class="control-label" for="bairro_empresa">Bairro</label> 
			<input type="text" class="form-control obrigatorio" id="bairro_empresa" name="bairro_empresa" placeholder="Bairro" aviso="Bairro" value="<?php echo $this->session->flashdata('bairro_empresa_edicao'); ?>" maxlength="20">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-7">
		<div class="form-group has-feedback">
			<label class="control-label" for="rua_empresa">Rua</label> 
			<input type="text" class="form-control obrigatorio" id="rua_empresa" name="rua_empresa" placeholder="Rua" aviso="Rua" value="<?php echo $this->session->flashdata('rua_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-2">
		<label class="control-label" for="numero_empresa">Número</label> 
		<input type="text" class="form-control" id="numero_empresa" name="numero_empresa" placeholder="Número" aviso="Número" value="<?php echo $this->session->flashdata('numero_empresa_edicao'); ?>" maxlength="10">
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="complemento_empresa">Complemento</label> 
			<input type="text" class="form-control" id="complemento_empresa" name="complemento_empresa" placeholder="Complemento" aviso="Complemento" value="<?php echo $this->session->flashdata('complemento_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

</div>


<div class="row">

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="telefone_empresa">Telefone</label> 
			<input type="text" class="form-control mascara_cel obrigatorio" id="telefone_empresa" name="telefone_empresa" placeholder="Telefone" aviso="Telefone" value="<?php echo $this->session->flashdata('telefone_empresa_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="ramal_empresa">Ramal</label> 
			<input type="text" class="form-control validar_numeros" id="ramal_empresa" name="ramal_empresa" placeholder="Ramal" aviso="Ramal" value="<?php echo $this->session->flashdata('ramal_empresa_edicao'); ?>" maxlength="10">
		</div>
	</div>

	<div class="col-md-2">
		<div class="form-group has-feedback">
			<label class="control-label" for="fax_empresa">Fax</label> 
			<input type="text" class="form-control mascara_cel" id="fax_empresa" name="fax_empresa" placeholder="Fax" aviso="Fax" value="<?php echo $this->session->flashdata('fax_empresa_edicao'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="homePage_empresa">Site</label> 
			<input type="text" class="form-control" id="homePage_empresa" name="homePage_empresa" placeholder="Site" aviso="Site" value="<?php echo $this->session->flashdata('homePage_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="email_empresa">E-mail</label> 
			<input type="text" class="form-control validar_email obrigatorio" id="email_empresa" name="email_empresa" placeholder="E-mail" aviso="E-mail" value="<?php echo $this->session->flashdata('email_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

</div>

<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="nome_responsavel_empresa">Nome Responsável</label> 
			<input type="text" class="form-control " id="nome_responsavel_empresa" name="nome_responsavel_empresa" placeholder="Nome Responsável" aviso="Nome Responsável" value="<?php echo $this->session->flashdata('nome_responsavel_empresa'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cel_responsavel_empresa">Telefone Responsável</label> 
			<input type="text" class="form-control mascara_cel" id="cel_responsavel_empresa" name="cel_responsavel_empresa" placeholder="Telefone Responsável" aviso="Telefone Responsável" value="<?php echo $this->session->flashdata('cel_responsavel_empresa'); ?>">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="cpf_responsavel_empresa">CPF Responsável</label> 
			<input type="text" class="form-control mascara_cpf validar_cpf" id="cpf_responsavel_empresa" name="cpf_responsavel_empresa" placeholder="CPF Responsável" aviso="CPF Responsável" value="<?php echo $this->session->flashdata('cpf_responsavel_empresa'); ?>">
		</div>
	</div>

</div>


<div class="row">

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="atividade1_empresa">Atividade 1</label> 
			<input type="text" class="form-control obrigatorio" id="atividade1_empresa" name="atividade1_empresa" placeholder="Atividade 1" aviso="Atividade 1" value="<?php echo $this->session->flashdata('atividade1_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="atividade2_empresa">Atividade 2</label> 
			<input type="text" class="form-control" id="atividade2_empresa" name="atividade2_empresa" placeholder="Atividade 2" aviso="Atividade 2" value="<?php echo $this->session->flashdata('atividade2_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<div class="form-group has-feedback">
			<label class="control-label" for="atividade3_empresa">Atividade 3</label> 
			<input type="text" class="form-control" id="atividade3_empresa" name="atividade3_empresa" placeholder="Atividade 3" aviso="Atividade 3" value="<?php echo $this->session->flashdata('atividade3_empresa_edicao'); ?>" maxlength="100">
		</div>
	</div>

	<div class="col-md-3">
		<label class="control-label" for="status_empresa">Status</label> 
		<select class="form-control obrigatorio" id="status_empresa" name="status_empresa">
			<option value="1">Ativa</option>
			<option value="0">Inativa</option>
		</select>
	</div>

</div>

<hr>

<div class="row finalizar_formulario">
	<div class="col-md-9"></div>
	<div class="col-md-3" align="right">
		<button type="button" class="btn btn-danger" title="Limpar os dados dos campos" id="apagar"> <i class="glyphicon glyphicon-trash"></i> Limpar</button>
		<button type="submit" class="btn btn-success" id="validar_Enviar" title="Criar"> <i class="glyphicon glyphicon-floppy-disk"></i> Editar </button>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-10"></div>
	<div class="col-md-2" align="right" style="padding-bottom: 20px;">
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Adicionar Filial.</button>
	</div>
</div>

<?php if (count($dados_iniciais['filiais']) > 0): ?>

	<table class="table table-bordered table-hover" align="center">
		<thead align="center">
			<th style="width: 60px;" align="center" class="no-filter">Editar</th>
			<th>Nome</th>
			<th>Sigla</th>
		</thead>
		<tbody align="center">	
		<?php 

			foreach ($dados_iniciais['filiais'] as $filial) {
				echo '<tr>';

				echo '<td style="width: 100px;">
						<a class="btn btn-success edtFilial" cod="'.$filial->id_filial.'"> 
							<i class="glyphicon glyphicon-edit"> </i> Editar
						</a>
					</td>';
		
				echo '<td><span style="display: none">'.$filial->nome_filial.'</span><input class="form-control" type="text" class="nome_filial" id="nome_filial_'.$filial->id_filial.'" value="'.$filial->nome_filial.'"></td>';
				echo '<td style="width: 160px;"><span style="display: none">'.$filial->sigla_filial.'</span><input class="form-control" type="text" class="sigla_filial" maxlength="3" id="sigla_filial_'.$filial->id_filial.'" value="'.$filial->sigla_filial.'"></td>';

				echo '</tr>';
			}

		?>
		</tbody>
	</table>

<?php endif ?>

<?php echo form_close(); ?>

<hr>

<div class="row">
	<div class="col-md-10"></div>
	<div class="col-md-2" align="right" style="padding-bottom: 20px;">
		<button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModalMotorista">Adicionar Motorista.</button>
	</div>
</div>

<?php if (count($dados_iniciais['motoristas']) > 0): ?>

	<table class="table table-bordered table-hover" align="center">
		<thead align="center">
			<th style="width: 60px;" align="center" class="no-filter">Editar</th>
			<th>ID do Webservice</th>
			<th>Nome</th>
			<th>Filial</th>
			<th>Endereço</th>
		</thead>
		<tbody align="center">	
		<?php 

			foreach ($dados_iniciais['motoristas'] as $motorista) {
				echo '<tr>';

				echo '<td style="width: 100px;">
						<a class="btn btn-success edtMotorista" cod="'.$motorista->id_motorista.'" data-toggle="modal" data-target="#myModalMotoristaEditar"> 
							<i class="glyphicon glyphicon-edit"> </i> Editar
						</a>
					</td>';
		
				echo '<td id="id_ws_motorista_'.$motorista->id_motorista.'">'.$motorista->id_ws_motorista.'</td>';
				echo '<td id="nome_motorista_'.$motorista->id_motorista.'">'.$motorista->nome_motorista.'</td>';
				echo '<td id="endereco_motorista_'.$motorista->id_motorista.'">'.$motorista->endereco_motorista.'</td>';
				echo '<td id="nome_filial_motorista_'.$motorista->id_motorista.'" cod="'.$motorista->id_filial.'">'.$motorista->nome_filial.'</td>';

				echo '</tr>';
			}

		?>
		</tbody>
	</table>
		
	
<?php endif ?>

<?php echo form_close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#status_empresa').val(<?php echo $this->session->flashdata('status_empresa_edicao'); ?>).trigger('change');

		$(document).on('click','.edtFilial',function () {

			var id_filial = $(this).attr('cod');
			var nome_filial  = $('#nome_filial_'+id_filial).val();
			var sigla_filial = $('#sigla_filial_'+id_filial).val();

			$.ajax({
		        type: "post",
		        url: "<?php echo base_url(); ?>controller_empresa/ajax_Editar_Filial",
		        data:{id_filial: id_filial, 
		        	  nome_filial: nome_filial,
		        	  sigla_filial: sigla_filial},
		        error: function(returnval) {
		           mensagem('Erro',returnval,'error');
		        },
		        success: function (returnval) {
		        	if(returnval == 'sucesso') {
		        		mensagem('Atualizado com Sucesso','Dados Alterados','success');
		        	} else {
		        		mensagem('Falha na edição',returnval,'error');
		        	}

		        }
		    })

		});

		$(document).on('click','#edtMotorista',function () {

			var id_motorista  		= $('#id_motorista').val();
			var id_ws_motorista  	= $('#id_ws_motorista').val();
			var nome_motorista  	= $('#nome_motorista').val();
			var endereco_motorista  = $('#endereco_motorista').val();
			var fk_filial 		 	= $('#fk_filial').val();

			$.ajax({
		        type: "post",
		        url: "<?php echo base_url(); ?>controller_empresa/ajax_Editar_Motorista",
		        data:{id_motorista: id_motorista, 
		        	  id_ws_motorista: id_ws_motorista,
		        	  nome_motorista: nome_motorista,
		        	  endereco_motorista: endereco_motorista,
		        	  fk_filial: fk_filial},
		        error: function(returnval) {
		           mensagem('Erro',returnval,'error');
		        },
		        success: function (returnval) {
		        	if(returnval == 'sucesso') {
		        		mensagem('Atualizado com Sucesso','Dados Alterados','success');
		        	} else {
		        		mensagem('Falha na edição',returnval,'error');
		        	}

		        }
		    })


		});

		
		$(document).on('click','.edtMotorista',function () {
		  	
			var cod = $(this).attr('cod');
			console.log("Teste");

			console.log($('#nome_filial_motorista_'+cod).attr('cod'));
			console.log('#nome_filial_motorista_'+cod);

			$('#id_ws_motorista').val(
				$('#id_ws_motorista_'+cod).text()
			);

			$('#nome_motorista').val(
				$('#nome_motorista_'+cod).text()
			);

			$('#endereco_motorista').val(
				$('#endereco_motorista_'+cod).text()
			);

			$('#id_motorista').val(
				cod
			);

			$('#fk_filial').val($('#nome_filial_motorista_'+cod).attr('cod')).trigger('change');


		})


		function mensagem(titulo,corpo,tipo) {
			$.toast({
			    heading: titulo,
			    text: corpo,
			    showHideTransition: 'slide',
			    position: 'top-center',
			    icon: tipo
			})
		}

	});
</script>

