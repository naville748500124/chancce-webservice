<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Grupos</h1>
	</div>
	<div class="col-md-4" align="right">
		<a class="btn btn-success" href="<?php echo base_url(); ?>main/redirecionar/6">
			<i class="glyphicon glyphicon-plus-sign"></i> Novo Grupo
		</a>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th style="width: 60px;" align="center" class="no-filter">Editar</th>
		<th>Nome</th>
		<th>Descrição</th>
		<th>Total de Grupos</th>
	</thead>
	<tbody align="center">	
	<?php 

		foreach ($dados_iniciais['grupos'] as $grupo) {
			echo '<tr>';

			echo '<td style="width: 60px;">
					<a class="btn btn-success" href="'.base_url().'main/redirecionar/4/'.$grupo->id_grupo.'"> <i class="glyphicon glyphicon-edit"> </i> Editar
					</button>
				</td>';

	
			echo '<td>'.$grupo->nome_grupo.'</td>';
			echo '<td>'.$grupo->descricao_grupo.'</td>';
			echo '<td>'.$grupo->total.'</td>';

			echo '</tr>';
		}

	?>
	</tbody>
</table>
