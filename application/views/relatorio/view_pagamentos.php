<div class="row">
	<div class="col-md-8">
		<h1> <i class="glyphicon glyphicon-list-alt"></i> Pagamentos</h1>
	</div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
	<thead align="center">
		<th>ID</th>
        <th>Nome</th>
        <th>Telefone</th>
        <th>Banco</th>
        <th>Agência</th>
        <th>Conta</th>
        <th>CPF</th>
        <th>Pagamentos Futuros</th>
        <th>Ação</th>
	</thead>
	<tbody align="center">	
	<tr>
            <th scope="row">#55</th>
            <td>Pedro Teles</td>
            <td>(11)4444-4444</td>
            <td>Itaú</td>
            <td>1111</td>
            <td>4555551111</td>
            <td>541.652.896-65</td>
            <td>R$10,00</td>
            <td>
                <form class="pure-form pure-form-aligned">

                    <button type="button" class="btn btn-success">Pagar
                    </button>

                </form>
            </td>


        </tr>

        <tr>
            <th scope="row">#15</th>
            <td>Rafael de Lima</td>
            <td>(11)1111-1111</td>
            <td>Santander</td>
            <td>5555</td>
            <td>5555522222</td>
            <td>256.254.111-10</td>
            <td>R$25,00</td>
            <td>
                <form class="pure-form pure-form-aligned">

                    <button type="button" class="btn btn-success">Pagar
                    </button>

                </form>
            </td>


        </tr>

        <tr>
            <th scope="row">#86</th>
            <td>Marcos Barbosa</td>
            <td>(11)2222-2222</td>
            <td>Bradesco</td>
            <td>8888</td>
            <td>8888855555</td>
            <td>258.963.333-30</td>
            <td>R$12,00</td>
            <td>
                <form class="pure-form pure-form-aligned">

                    <button type="button" class="btn btn-success">Pagar
                    </button>

                </form>
            </td>


        </tr>
	</tbody>
</table>
