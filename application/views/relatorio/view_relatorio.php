<div class="row">
    <div class="col-md-8">
        <h1> <i class="glyphicon glyphicon-list-alt"></i> Relatórios</h1>
    </div>
</div>

<hr>

<table class="table table-bordered table-hover" align="center">
    <thead align="center">
    <tr>
        <th>Filial</th>
        <th>Motorista</th>
        <th style="width: 60px;" align="center" class="no-filter">Corretos</th>
        <th style="width: 60px;" align="center" class="no-filter">Incorretos</th>
        <th style="width: 60px;" align="center" class="no-filter">Total</th>
    </tr>
    
    </thead>
    <tbody align="center">

    <?php
    

    foreach ($dados_iniciais as $empresa) {
        echo '<tr>';

        echo '<td>'.$empresa->nome_filial.'</td>';
        echo '<td>'.$empresa->nome_motorista.'</td>';

        if (is_null($empresa->acertos)) {
            echo '<td>0</td>';
        } else {
            echo '<td>'.$empresa->acertos.'</td>';
        }

        $incorretos = ($empresa->total - $empresa->acertos);

        if (is_null($incorretos)) {
            echo '<td>0</td>';
        } else {
            echo '<td>'.$incorretos.'</td>';
        }

        if (is_null($empresa->total)) {
            echo '<td>0</td>';
        } else {
            echo '<td>'.$empresa->total.'</td>';
        }

        echo '</tr>';
    }
    
    ?>
    </tbody>
</table>

<div class="row" style="margin-left: -30px;">

    <div class="col-md-1" align="center">
         <a id="excel" target="_blank" style="cursor: pointer;">
          <img src="<?php echo base_url() ?>style/img/excel.jpg">
        </a>
    </div>
    <div class="col-md-1" style="margin-left: -20px;" style="cursor: pointer;" align="left">
        <a href="<?php echo base_url() ?>Controller_relatorios/pdf_relatorios">
            <img src="<?php echo base_url() ?>style/img/pdf.jpg">
        </a>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#excel').click(function(){

            var url = "<?php echo base_url(); ?>Controller_relatorios/excel_lista";
            $.ajax({
                url: url,
                type: "post",
                datatype: 'json',
                success: function(data){
                
                    window.location = url;

                },
              
                error:function(){
                  
              }   
            });

        });

    });
</script>
