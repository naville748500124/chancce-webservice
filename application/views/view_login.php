<!DOCTYPE html>
<html lang="pt-br">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="description" content="Shekparts">
  <meta name="author" content="Naville Marketing">
  <link rel="icon" href="<?php echo base_url(); ?>style/img/chancce_fav.png">

  <title>Chancce | Login</title>

  <link type="text/css" href="<?php echo base_url(); ?>style/css/bootstrap.min.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/login.css" rel="stylesheet" />
  <link type="text/css" href="<?php echo base_url(); ?>style/css/jquery.toast.css" rel="stylesheet" />
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.mask.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/bootstrap.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>style/js/jquery.toast.js"></script>

  <!-- Específico do Chancce -->
  <link href="<?php echo base_url(); ?>style/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>style/css/starter-template.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>style/css/estilo_login.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>style/css/hover.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>style/css/animate.css" rel="stylesheet">

  <script src="<?php echo base_url(); ?>style/js/ie8-responsive-file-warning.js"></script>
  <script src="<?php echo base_url(); ?>style/js/ie-emulation-modes-warning.js"></script>

  <!-- CDN -->
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <!-- animacoes teste -->
  <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>
  <!-- fontes externas -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

<body style="background-image: url('<?php echo base_url() ?>style/img/background.jpg')">

<center><img src="<?php echo base_url() ?>style/img/logobranco.png" width="300px" style="margin-top: 15%;margin-bottom: 15px;"></center>

<center>
    <form method="post" action="<?php echo base_url(); ?>main/login">
        <input type="email" id="login" class="login-chancce validar_email" name="email" placeholder="E-mail" size="60" maxlength="49"><br>

        <input type="password" class="login-chancce" placeholder="Senha" name="senha" id="senha" size="60" maxlength="49"
               style="margin-top: 1%"><br>

        <button style="margin-top: 2%" name="logar" class="botao-login">Entrar</button>
    </form>
</center>

<script type="text/javascript">
  $(document).ready(function(){

    $('.acessar').click(function(){
        $('#login').hide("slow");
        $('#img_load').show("slow");
    });

    $(".mascara_cel").mask('(99) 99999-9999', {'translation': {
            A: {pattern: /[A-Za-z0-9]/}
          }
    },{reverse: true}); 

    var erro_email = false;

    $(".validar_email").focusout(function(){

      console.log('Validando E-mail...');

      if($(this).val() != "") { 

         var filtro = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

         if(!filtro.test($(this).val())) {
    
          toast('E-mail', 'E-mail inválido!','error',true);
          erro_email = true;
          $(this).focus();
          return false;

         } else {

          erro_email = false;
          $.toast().reset('all');

         }

      } else {

        erro_email = false;
        $.toast().reset('all');

      }

   });
   

    $('#esqueciSenha').click(function(){

      if ($('#usuario').val() != "") {

        $('#login').hide();
        $('#img_load').show();

        $.ajax({
              url: "<?php echo base_url(); ?>controller_usuarios/esqueci_Senha",
              type: "post",
              data: {login: $("#usuario").val()},
              datatype: 'json',
              success: function(data){

                if (data['status'] == 1) {
                  toast('Senha alterada com sucesso!',data['resultado'],'success',false);
                } else {
                  toast('Senha não alterada ou erro ao enviar E-mail!',data['resultado'],'error',true);
                }

                 
                $('#login').show();
            $('#img_load').hide();
               

              },
              error:function(){
                toast('Falha','Falha ao alterar senha.','error',true);

                $('#login').show();
                $('#img_load').hide();
                  
              }   
            });

      } else {
        toast('Digite o usuário.','Usuário em branco.','error',true);
      }

    });

    <?php 
      //Caso a tentativa de login falhe
      if(isset($falha)){

        echo "$.toast({
                heading: 'Falha no login',
                text: [
                    'Usuário e/ou senha inválidos', 
                    'Ou Login e/ou Grupo inativo',
                    'Entre em contato com um Administrador.'
                  ],
                hideAfter: false,
                position: 'top-right',
                icon: 'error'
            });";
      }

      //Saiu do sistema.
      if(isset($saiu)){
        //Aviso quando outro login foi realizado
        $forcado_ = "";
        if (isset($forcado) && $forcado != "") {
          $forcado_ = "Novo acesso registrado em: ".$forcado;
        }

        echo "toast('Até logo ".$saiu."!','Acesso encerrado. ".$forcado_."','success',false);";

      }

    ?>

   

    function toast(titulo,mensagem,tipo,fixo){

      $.toast().reset('all');

      if (fixo) {

         $.toast({
            heading: titulo,
            text: mensagem,
            hideAfter: false,
            position: 'top-right',
            icon: tipo
        });

      } else {

        $.toast({
            heading: titulo,
            text: mensagem,
            showHideTransition: 'fade',
            position: 'top-right',
            hideAfter : 5000,  
            icon: tipo
        });

      } 
    }

  });
  </script>