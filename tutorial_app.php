<html>
<head>
    <title>E-mail</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <style>
        body{
            font-family: 'Open Sans', sans-serif;
            margin: 0;
            padding: 0;
        }
    </style>
</head>
<body>
<p style="margin-top: 30px">&nbsp;</p>
<p style="text-align: center"><img src="style/img/chancce_logo_tuto.png" width="130px"></p>
<p style="margin-top: 30px">&nbsp;</p>
<p style="width: 60%;margin: 0 auto; text-align: justify">



    <?php
    echo('<h1 style="text-align: center;font-weight: bolder;font-size: 25px">COMO REALIZAR ESSA OPORTUNIDADE?</h1>');
    ?>


<p style="margin-top: 10px">&nbsp;</p>
<!--<p style="margin-top: 10px">&nbsp;</p>
<p style="text-align: center"><img src="style/img/qr.png" width="230px"></p>

<p style="margin-top: 10px">&nbsp;</p>
<p style="text-align: center;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    Este é o código QR reconhecido pelo nosso sistema, ele permite a autenticação do documento, impedindo fraude ou cópia desde documento.
</p> -->


<p style="margin-top: 10px">&nbsp;</p>
<p style="text-align: center"><img src="style/img/assinatura.png" width="60%"></p>

<p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    Verifique se a escrita corresponde ao exemplo mostrado na timeline e as marcações não estão fraudadas, tudo referente ao exemplo mostrado.
</p>



<p style="margin-top: 10px">&nbsp;</p>
<p style="text-align: center"><img src="style/img/campos.png" width="60%"></p>

<p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    Verifique a seleção receptor referente ao destino e compare com a assinatura ao lado e RG fornecido.
</p>


<p style="margin-top: 10px">&nbsp;</p>


    <p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    Campo em Branco: campos obrigatórios de nome, data, grau de parentesco, assinatura devem ser preenchidos obrigatoriamente.
    </p>
<p style="margin-top: 10px">&nbsp;</p>

        <p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    Campo Ilegível: campos obrigatórios de nome, data, grau de parentesco devem estar legíveis.

    <p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    Data Divergente: verificar se a data da entrega informada no SAP corresponde à data registrada no comprovante.
    </p>

<p style="margin-top: 10px">&nbsp;</p>
    <p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">
    SPP: pedidos SPP (grau de risco 3) devem ser obrigatoriamente conferidos e apenas pelo destinatário da NF.
    </p>
<p style="margin-top: 10px">&nbsp;</p>
    
    <p style="text-align: justify;width: 310px;margin: 0 auto;font-weight: bolder;color: #696969">   
RG/CPF: Natura precisa checar se esta informação estará disponível Campo preenchido pelo motorista => item verificado apenas para comprovantes da Dias através da função comparar.
</p>
</ul>

</p>
</body>
</html>