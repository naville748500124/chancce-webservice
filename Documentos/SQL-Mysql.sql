DROP DATABASE naville_chancce;
CREATE SCHEMA naville_chancce;
use naville_chancce;

CREATE TABLE IF NOT EXISTS cad_grupo_itens (

	id_grupo_item int not null AUTO_INCREMENT,
	nome_grupo_item text,
	descricao_grupo_item text,

	PRIMARY KEY (id_grupo_item)	

);

insert into cad_grupo_itens (id_grupo_item,nome_grupo_item,descricao_grupo_item) 
	values 
		(1,"Bancos","Lista dos bancos");

CREATE TABLE IF NOT EXISTS cad_itens (

	id_item int not null AUTO_INCREMENT,
	fk_grupo_item int,
	nome_item text,
	descricao_item text,

	PRIMARY KEY (id_item),
	FOREIGN KEY (fk_grupo_item) REFERENCES cad_grupo_itens (id_grupo_item)	

);

insert cad_itens (fk_grupo_item,nome_item,descricao_item) 
	values 
		(1,'001 - Banco do Brasil','Banco do Brasil'),
		(1,'033 - Santander','Santander'),
		(1,'104 - Caixa Econômica Federal','Caixa Econômica Federal'),
		(1,'237 - Bradesco','Bradesco'),
		(1,'341 - Itaú','Itaú'),
		(1,'422 - Safra','Safra');

/*Segurança Usuários*/
CREATE TABLE IF NOT EXISTS seg_usuarios(

	id_usuario int not null AUTO_INCREMENT comment "ID Usuário",
	nome_usuario character varying(100) NOT NULL comment "Nome Usuário",
	email_usuario character varying(64) NOT NULL comment "E-mail Usuário",
	telefone_usuario character varying(11) NOT NULL comment "Telefone Usuário",
	login_usuario character varying(20) NOT NULL comment "Login Usuário",
	senha_usuario character varying(40) NOT NULL comment "Senha Usuário",
	ativo_usuario boolean NOT NULL comment "Status Usuário",
	fk_grupo_usuario int not null comment "Grupo Usuário",
	usuario_criou_usuario int comment "Usuário que criou",
	criacao_usuario timestamp DEFAULT CURRENT_TIMESTAMP comment "Data criação",

	banco_usuario int comment "Banco",
	agencia_usuario character varying(8) comment "Agencia",
	conta_usuario character varying(16) comment "Conta",
	digito_usuario character varying(1) comment "Dígito Conta",
	cpf_usuario character varying(9) comment "CPF",
	img_usuario character varying(128) comment "Imagem",

	PRIMARY KEY (id_usuario),
	UNIQUE (email_usuario),
	UNIQUE (cpf_usuario),
	FOREIGN KEY (usuario_criou_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (banco_usuario) REFERENCES cad_itens (id_item)

);

/*Segurança Grupos*/
CREATE TABLE IF NOT EXISTS seg_grupos(

	id_grupo int not null AUTO_INCREMENT comment "ID Grupo",
	nome_grupo character varying(100) NOT NULL comment "Nome Grupo",
	descricao_grupo text NOT NULL comment "Descrição Grupo",
	usuario_criou_grupo int comment "Usuário que criou",
	ativo_grupo boolean NOT NULL comment "Status Grupo",
	criacao_grupo timestamp DEFAULT CURRENT_TIMESTAMP comment "Data Criação",

	PRIMARY KEY (id_grupo),
	CONSTRAINT unique_nome_grupo UNIQUE (nome_grupo),
	FOREIGN KEY (usuario_criou_grupo) REFERENCES seg_usuarios (id_usuario)

);

ALTER TABLE seg_usuarios
ADD FOREIGN KEY (fk_grupo_usuario) REFERENCES seg_grupos(id_grupo);

/*Segurança, cadastro dos models*/
CREATE TABLE IF NOT EXISTS seg_models(

	id_model int not null AUTO_INCREMENT,
	link_model character varying(100) not null,
	descricao_model text not null,

	CONSTRAINT pk_model PRIMARY KEY (id_model)

);

CREATE TABLE IF NOT EXISTS seg_log_acesso(

	id_log_acesso int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_acesso timestamp DEFAULT CURRENT_TIMESTAMP,
	ip_usuario_acesso character varying(16),
	acesso boolean,
	maquina_usuario_acesso text,

	PRIMARY KEY (id_log_acesso),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

CREATE TABLE IF NOT EXISTS seg_log_erro(

	id_log_erro int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_erro timestamp DEFAULT CURRENT_TIMESTAMP,
	cod text, /*Text para casos onde o código seja texto ao invés de número*/
	erro text,
	query text,
	erro_feedback text, /*Relatado pelo usuário*/
	funcao text,
	maquina_usuario_erro text,

	PRIMARY KEY (id_log_erro),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario)

);

/*Segurança, cadastro dos controllers*/
CREATE TABLE IF NOT EXISTS seg_controllers(

	id_controller int not null AUTO_INCREMENT,
	link_controller character varying(100) not null,
	descricao_controller text not null,
	fk_model int,

	PRIMARY KEY (id_controller),
	FOREIGN KEY (fk_model) REFERENCES seg_models (id_model)

);

/*Segurança menu, gerá um menu no aplicativo, as telas devem ser vinculadas*/
CREATE TABLE IF NOT EXISTS seg_menu(

	id_menu int not null AUTO_INCREMENT,
	titulo_menu character varying(100) NOT NULL,
	descricao_menu text NOT NULL,
	menu_acima int, 
	posicao_menu int,

	PRIMARY KEY (id_menu),
	FOREIGN KEY (menu_acima) REFERENCES seg_menu (id_menu)

);

/*Segurança, aplicações do menu.*/
CREATE TABLE IF NOT EXISTS seg_aplicacao(

	id_aplicacao int not null AUTO_INCREMENT,
	link_aplicacao character varying(100) NOT NULL,
	titulo_aplicacao character varying(100) NOT NULL,
	descricao_aplicacao text NOT NULL,
	fk_controller int,

	PRIMARY KEY (id_aplicacao),
    FOREIGN KEY (fk_controller) REFERENCES seg_controllers (id_controller)

);

/*Gera um Log dos acessos do usuário.*/
CREATE TABLE IF NOT EXISTS seg_log_navegacao(

	id_log_navegacao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_navegacao timestamp DEFAULT CURRENT_TIMESTAMP,
	permissao boolean,
	fk_aplicacao int,
	parametros text,

	PRIMARY KEY (id_log_navegacao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

CREATE TABLE IF NOT EXISTS seg_log_edicao(

	id_log_edicao int NOT NULL AUTO_INCREMENT,
	fk_usuario int,
	data_log_edicao timestamp DEFAULT CURRENT_TIMESTAMP,
	original_edicao text,
	novo_edicao text,
	campo_edicao text,
	tabela_edicao text,
	fk_aplicacao int,
	id_edicao int,
	

	PRIMARY KEY (id_log_edicao),
	FOREIGN KEY (fk_usuario) REFERENCES seg_usuarios (id_usuario),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao)

);

/*Vincula as aplicações ao menu*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_menu(

	id_aplicacoes_menu int not null AUTO_INCREMENT,
	fk_aplicacao int NOT NULL,
	fk_menu int NOT NULL,

	PRIMARY KEY (id_aplicacoes_menu),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_menu) REFERENCES seg_menu (id_menu)

);

/*Segurança, Grupo aplicação*/
CREATE TABLE IF NOT EXISTS seg_aplicacoes_grupos(

	id_aplicacoes_grupos int not null AUTO_INCREMENT,
	fk_grupo int NOT NULL,
	fk_aplicacao int NOT NULL,

	PRIMARY KEY (id_aplicacoes_grupos),
	FOREIGN KEY (fk_aplicacao) REFERENCES seg_aplicacao (id_aplicacao),
    FOREIGN KEY (fk_grupo) REFERENCES seg_grupos (id_grupo)

);

/*Cadastro dos controllers e models*/
insert into seg_models (id_model,link_model,descricao_model) values 
	(1,'Model_menu','Responsável pelos menus.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(2,'Model_seguranca','Responsável pelo acesso as sistema e controle de perfils.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(3,'Model_usuarios','Responsável pelo gerênciamento dos usuários.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(4,'Model_grupos','Responsável pelo gerênciamento dos grupos.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(5,'Model_oportunidades','Responsável pelo gerênciamento dos Oportunidade.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(6,'Model_empresas','Responsável pelo gerênciamento dos Empresas.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(7,'Model_pagamentos','Responsável pelo gerênciamento dos Pagamentos.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(8,'Model_auditoria','Responsável pelo gerênciamento das pós auditorias.');
insert into seg_models (id_model,link_model,descricao_model) values 
	(9,'Model_relatorios','Responsável pelo gerênciamento dos relatórios.');


insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(1,'Main','Responsável pelo gerênciamento do acesso ao sistema.',2);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(2,'Controller_usuarios','Responsável pelo gerênciamento dos usuários.',3);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(3,'Controller_grupos','Responsável pelo gerênciamento dos grupos.',4);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(4,'Controller_oportunidades','Responsável pelo gerênciamento dos oportunidade.',5);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(5,'Controller_empresas','Responsável pelo gerênciamento dos empresas.',6);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(6,'Controller_pagamentos','Responsável pelo gerênciamento dos pagamentos.',7);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(7,'Controller_auditoria','Responsável pelo gerênciamento das pós auditorias.',8);
insert into seg_controllers (id_controller,link_controller,descricao_controller,fk_model) values 
	(8,'Controller_relatorios','Responsável pelo gerênciamento dos relatórios.',9);


/*INSERTS PARA USUÁRIO DE TESTES*/
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (1,'Administradores','Administradores do sistema',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (2,'Usuários Comuns','Usuários do APP',true);
insert into seg_grupos(id_grupo,nome_grupo,descricao_grupo,ativo_grupo) values (3,'Usuários empresa','Usuários da empresa',true);

insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario)
values (1,'Usuário Naville','naville@chancce.com','11962782329','naville','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,1);
insert into seg_usuarios (id_usuario,nome_usuario,email_usuario,telefone_usuario,login_usuario,senha_usuario,ativo_usuario,fk_grupo_usuario)
values (2,'Usuário Naville','teste@chancce.com','11962782329','naville2','40bd001563085fc35165329ea1ff5c5ecbdbbeef',TRUE,3);

/*Menus*/
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (1,'Oportunidades','Oportunidades',null,1);	 

insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (2,'Cadastre','Cadastre',null,2);	 

insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (3,'Pagamentos','Pagamentos',null,3);	 

insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (4,'Configurações','Configurações',null,4);	 

insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (11,'Auditar','Pós auditoria do canhoro',null,0);	 

insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (12,'Relatórios','Relatórios',null,3);	 

/*Sub Menus*/
-- Oportunidade
-- insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
-- 	values (5,'Nova Oportunidade','Criar oportunidade',1,null);
-- insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
-- 	values (6,'Oportunidades','Lista de Oportunidades',1,null);
-- Cadastre
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (7,'Nova Empresa','Nova Empresa',2,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (8,'Novo Usuário','Novo Usuário',2,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (9,'Empresas','Lista de empresas',2,null);
insert into seg_menu (id_menu,titulo_menu,descricao_menu,menu_acima,posicao_menu) 
	values (10,'Lista de usuários','Lista de usuários',2,null);

/*Difinindo aplicações e links*/
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (1,'oportunidades/view_oportunidades','Listar Oportunidades','Listar Oportunidades',4);
-- insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
-- 	values (2,'oportunidades/view_nova_oportunidade','Nova Oportunidade','Nova Oportunidade',4);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (3,'oportunidades/view_editar_oportunidade','Editar Oportunidade','Editar Oportunidade',4);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (4,'empresa/view_empresas','Listar Empresa','Listar Empresa',5);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (5,'empresa/view_nova_empresa','Nova Empresa','Nova Empresa',5);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (6,'empresa/view_editar_empresa','Editar Empresa','Editar Empresa',5);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (7,'seguranca/view_usuarios','Listar Usuários','Listar Usuários',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (8,'seguranca/view_novo_usuario','Novo Usuário','Novo Usuário',2);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (9,'seguranca/view_editar_usuario','Editar Usuário','Editar Usuário',2);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (10,'relatorio/view_pagamentos','Relatório de pagamentos','Relatório de pagamentos',6);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (11,'seguranca/view_editar_perfil','Editar Perfil','Editar Perfil',2);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (12,'auditoria/view_auditar','Audita Canhoro','Audita Canhoro',7);
insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (13,'auditoria/view_audicoes','Lista auditados','Lista auditados',7);

insert into seg_aplicacao (id_aplicacao,link_aplicacao,titulo_aplicacao,descricao_aplicacao,fk_controller) 
	values (14,'relatorio/view_relatorio','Relatórios','Relatórios',8);


/*Indicando quais aplicações estão ligadas a quais menus.*/
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (1,1);
-- insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
-- 	values (2,5);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (4,9);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (5,7);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (7,10);
insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (8,8);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (10,3);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (11,4);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (13,11);

insert into seg_aplicacoes_menu(fk_aplicacao,fk_menu)
	values (14,12);

/*Dando permissões para o grupo administrador.*/
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(1,1); 
-- insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
-- 	values(2,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(3,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(4,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(5,1); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(6,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(7,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(8,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(9,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(10,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(11,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(12,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(13,1);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(14,1);

insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(1,3); 
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(12,3);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(13,3);
insert into seg_aplicacoes_grupos(fk_aplicacao,fk_grupo)
	values(14,3);
/*Chancce */

CREATE TABLE IF NOT EXISTS cad_empresa (

	id_empresa int not null AUTO_INCREMENT,
	
	razao_social_empresa character varying(100),
	nome_fantasia_empresa character varying(100),
	cnpj_empresa character varying(14),
	inscricao_estadual_empresa character varying(100),

	rua_empresa character varying(100),
	numero_empresa character varying(10),
	complemento_empresa character varying(100),
	bairro_empresa character varying(20),
	cep_empresa character varying(8),
	cidade_empresa character varying(20),
	uf_empresa character varying(2),

	telefone_empresa character varying(11),
	ramal_empresa character varying(10),
	fax_empresa character varying(11),
	homePage_empresa character varying(100),
	email_empresa character varying(100),

	atividade1_empresa character varying(100),
	atividade2_empresa character varying(100),
	atividade3_empresa character varying(100),
	data_cadastro_empresa timestamp DEFAULT CURRENT_TIMESTAMP,
	usuario_criou_empresa int,
	status_empresa boolean,

	PRIMARY KEY (id_empresa)

);

-- CREATE TABLE IF NOT EXISTS cad_blocos (

-- );


-- CREATE TABLE IF NOT EXISTS cad_canhotos (

-- 	id_canhoto int not null AUTO_INCREMENT,
-- 	ws_id_canhoto int,
-- 	ws_imagem_canhoto character varying(100),
-- 	ws_sigla_canhoto character varying(5),
-- 	ws_filial_canhoto character varying(20),
-- 	ws_motorista_canhoto character varying(50),

-- 	data_canhoto date DEFAULT current_date,
-- 	fk_bloco_canhoto int,
-- 	fk_empresa_canhoto int, /*ANALISAR*/
-- 	status_canhoto int,
-- 	fk_auditor int,
-- 	fk_pos_auditor int,
-- 	valor_canhoto real,
-- 	erros_canhoto int,

-- 	PRIMARY KEY (id_canhoto)

-- );
























